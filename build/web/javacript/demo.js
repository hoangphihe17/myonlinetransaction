     const notifications = document.querySelector(".notifications");

                const toastDetails = {
                    timer: 3000,
                    success: {
                        icon: 'fa-circle-check',
                        text: 'Success: ' // Cập nhật thành công
                    },
                    error: {
                        icon: 'fa-circle-xmark',
                        text: 'Error: '// Cập nhật có lỗi
                    },
                    warning: {
                        icon: 'fa-triangle-exclamation',
                        text: 'Warning: ' // Cập nhật cảnh báo
                    },
                    info: {
                        icon: 'fa-circle-info',
                        text: 'Info: ' // Cập nhật thông tin
                    }
                };

                const removeToast = (toast) => {
                    toast.classList.add("hide");
                    if (toast.timeoutId)
                        clearTimeout(toast.timeoutId);
                    setTimeout(() => toast.remove(), 500);
                };

    // Cập nhật hàm createToast để nhận thêm một tham số message
                const createToast = (id, message) => {
                    const {icon, text} = toastDetails[id];
                    const toast = document.createElement("li");
                    toast.className = `toast ${id}`;
                    toast.innerHTML = `<div class="column">
                             <i  class="fa-solid ${icon}"></i>
                             <span style="color:black;">${text + message}</span>
                          </div>
                          <i class="fa-solid fa-xmark" onclick="removeToast(this.parentElement)"></i>`;
                    notifications.appendChild(toast);
                    toast.timeoutId = setTimeout(() => removeToast(toast), toastDetails.timer);
                };


 