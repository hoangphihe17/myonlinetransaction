<%-- 
    Document   : bookmark
    Created on : Mar 1, 2024, 9:49:42 AM
    Author     : Truong cun
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>bookmark</title>
        <style>
            *{
                margin: 0;
                padding: 0;
                font-family:Verdana, Geneva, Tahoma, sans-serif;
                box-sizing: border-box;
            }

            body{
                background: #2e364a;
            }

            .timeline{
                position: relative;
                max-width: 1200px;
                margin: 100px auto;
            }

            .container{
                padding: 10px 50px;
                position: relative;
                width: 50%;
                animation: movedown 1s linear forwards;
                opacity: 0;
            }

            @keyframes movedown {
                0%{
                    opacity: 1;
                    transform:  translateY(-30px);
                }
                100%{
                    opacity: 1;
                    transform: translateY(0px);
                }
            }

            .container:nth-child(1){
                animation-delay: 0s;
            }
            .container:nth-child(2){
                animation-delay: 1s;
            }
            .container:nth-child(3){
                animation-delay: 2s;
            }
            .container:nth-child(4){
                animation-delay: 3s;
            }
            .container:nth-child(5){
                animation-delay: 4s;
            }
            .container:nth-child(6){
                animation-delay: 5s;
            }
            .text-box{
                padding: 20px 30px;
                background: #fff;
                position: relative;
                border-radius: 6px;
                font-size: 15px;
            }


            .left-container{
                left: 0;
            }

            .right-container{
                left: 50%;
            }

            .container img{
                position: absolute;
                width: 40px;
                border-radius: 50%;
                right: -20px;
                top: 32px;
                z-index: 10;
            }

            .right-container img{
                left: -20px;
            }

            .timeline::after{
                content: '';
                position: absolute;
                width: 6px;
                height: 100%;
                background: #fff;
                top: 0;
                left: 50%;
                margin-left: -3px;
                z-index: -1;
                animation: moveline 6s linear forwards;
            }

            @keyframes moveline {
                0%{
                    height: 0;
                }
                100%{
                    height: 100%;
                }
            }
            .text-box h2{
                font-weight: 600;
            }

            .text-box small{
                display: inline-block;
                margin-bottom: 15px;
            }

            .left-container-arrow{
                height: 0;
                width: 0;
                position: absolute;
                top: 28px;
                z-index: 1;
                border-top: 15px solid transparent;
                border-bottom: 15px solid transparent;
                border-left:  15px solid #fff;
                right: -15px;
            }

            .right-container-arrow{
                height: 0;
                width: 0;
                position: absolute;
                top: 28px;
                z-index: 1;
                border-top: 15px solid transparent;
                border-bottom: 15px solid transparent;
                border-right:  15px solid #fff;
                left: -15px;
            }

            @media screen  and  (max-width: 600px){
                .timeline{
                    margin: 50px auto;
                }
                .timeline::after{
                    left: 31px;
                }
                .container{
                    width: 100%;
                    padding-left: 80px;
                    padding-right: 25px;
                }
                .text-box{
                    font-size: 13px;
                }
                .text-box small{
                    margin-bottom: 10px;
                }
                .right-container{
                    left: 0;
                }
                .left-container img, .right-container img{
                    left: 10px;
                }
                .left-container-arrow, .right-container-arrow{
                    border-right: 15px solid #fff;
                    border-left: 0;
                    left: -15px;
                }

            }
        </style>
    </head>
    <body>
        <!--ID: ${product.id}, Tên: ${product.productName}, Mô tả: ${product.describe}, Giá: ${product.price}, Thông tin:--> 
        <c:if test="${favoriteProducts==null}">
            List bookmark empty 
        </c:if>
            
        <div class="timeline">

            <c:forEach var="product" items="${favoriteProducts}" varStatus="loop">
                
               
                
                
                <c:if test="${((loop.index + 1)%2)!=0}">
                    <div class="container left-container">
                        <img src="image/akaza.jpg">
                        <div class="text-box">
                            <input hidden value="${product.id}">
                            <h2> ${product.productName}</h2>
                            <small>${product.createAt}</small>
                            <p> ${product.describe}</p>
                            <p> ${product.information}</p>
                            <p> ${product.price}</p>
                            <span class="left-container-arrow"></span>
                        </div>
                    </div>
                </c:if>

                <c:if test="${((loop.index + 1)%2) ==0}">
                    <div class="container right-container">
                        <img src="images/google.png">
                        <div class="text-box">
                            <input hidden value="${product.id}">
                            <h2> ${product.productName}</h2>
                            <small>2018 - 2019</small>
                            <p> ${product.describe}</p>
                            <p> ${product.information}</p>
                            <p> ${product.price}</p>
                            <span class="left-container-arrow"></span>
                        </div>
                    </div>
                </c:if>

            </c:forEach>



          
        </div> 

    </body>
</html>
