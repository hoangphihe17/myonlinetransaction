<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Custom Select Input - InCoder</title>
        <link rel="stylesheet" href="main.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <style>

            @import url("https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap");

            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: "Poppins", sans-serif;
            }

            body {
                display: flex;
                justify-content: center;
                background-color: rgb(52, 211, 153);
            }

            .customInputContainer_123 {
                width: 20rem;
                display: flex;
                max-width: 20rem;
                margin-top: 2rem;
                align-items: center;
                flex-direction: column;
                justify-content: center;
            }

            .customInputContainer_123 .customInput_123 {
                margin: 0 1rem;
                cursor: pointer;
                user-select: none;
                font-size: 1.16rem;
                justify-content: space-between;
                padding: 0.6rem 1rem 0.6rem 0.6rem;
            }

            .customInputContainer_123 .customInput_123 i {
                transition: transform 0.3s ease-in-out;
            }

            .customInputContainer_123.show .customInput_123 i {
                transform: rotate(90deg);
            }

            .customInputContainer_123 :is(.customInput_123, .options_123) {
                width: 100%;
                display: flex;
                color: #202020;
                background: #fff;
                align-items: center;
                border-radius: 0.3rem;
            }

            .customInputContainer_123 .options_123 {
                display: none;
                padding: 0.6rem;
                font-size: 1.1rem;
                justify-content: start;
                flex-direction: column;
                margin: 0.8rem 1rem 0rem 1rem;
                transition: background-color 0.1s ease-in-out;
            }

            .customInputContainer_123.show .options_123 {
                display: block;
            }

            .customInputContainer_123 .options_123 :is(.searchInput_123, ul) {
                width: 100%;
                max-height: 15rem;
                overflow-y: scroll;
                position: relative;
            }

            .customInputContainer_123 .options_123 ul::-webkit-scrollbar {
                width: 6px;
                position: relative;
            }

            .customInputContainer_123 .options_123 ul::-webkit-scrollbar-track{
                width: 2px;
                border-radius: .2rem;
                background: rgb(0 0 0 / 10%)
            }

            .customInputContainer_123 .options_123 ul::-webkit-scrollbar-thumb{
                border-radius: .2rem;
                background: rgb(0 0 0 / 30%);
            }

            .customInputContainer_123 .options_123 .searchInput_123 {
                display: flex;
                padding: 0 0.4rem;
                overflow-y: auto;
                align-items: center;
                border-radius: 0.4rem;
                color: rgb(0 0 0 / 50%);
                border: 2px solid rgb(0 0 0 / 30%);
            }

            .customInputContainer_123 .options_123 .searchInput_123.focus {
                border: 2px solid rgb(52 211 153 / 70%);
            }

            .customInputContainer_123 .options_123 .searchInput_123 input[type="text"] {
                border: 0;
                width: 100%;
                outline: none;
                height: 2.5rem;
                font-size: 1rem;
                padding: 0 0.4rem;
                border-radius: 0.4rem;
            }

            .customInputContainer_123 .options_123 .searchInput_123 input[type="text"]::placeholder {
                font-size: 1rem;
                color: rgb(0 0 0 / 50%);
            }

            .customInputContainer_123 .options_123 ul {
                margin: 0.5rem 0;
            }

            .customInputContainer_123 .options_123 ul li {
                cursor: pointer;
                list-style: none;
                padding: 0.4rem 0.4rem;
                border-bottom: 1px solid rgb(204 204 204 / 50%);
            }

            .customInputContainer_123 .options_123 ul li.selected {
                background: rgb(52 211 153 / 50%);
            }

            .customInputContainer_123 .options_123 ul li.selected:hover {
                background: rgb(52 211 153 / 50%);
            }
            img{
                width: 29px;

            }
            li{
                display: flex;
                gap :5px;
            }

        </style>


    </head>




    <body>
        <form>
            <div class="customInputContainer_123 show">
                <div class="customInput_123">
                    <div style="display: flex; gap: 5px" class="selectedData_123">
                        <img src="image/truongcun.png"> Truong Cun
                    </div>
                    <i class="fa-solid fa-angle-right"></i>
                    <input hidden name="findUser" value="0">
                </div>
                <div class="options_123">
                    <div class="searchInput_123">
                        <i class="fa-solid fa-magnifying-glass"></i>
                        <input type="text" id="searchInput_123" name="key" placeholder="Search">
                    </div>
                    <ul id="loadfindTransfer">
                        <c:if test="${findTranfer==null}">
                            <p>
                                Try searching something else.

                            </p>
                        </c:if>
                        <c:forEach var="p" items="${findTranfer}">
                            <li class="custom-cursor-on-hover">
                                <img src="${p.avatar}">
                                <p>${p.userName}</p>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </form>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.3.js"
        integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

        <script>




            let customInput_123 = document.querySelector('.customInput_123');
            selectedData_123 = document.querySelector('.selectedData_123');
//            searchInput_123 = document.querySelector('.searchInput_123 input');
            ul_123 = document.querySelector('.options_123 ul');
            customInputContainer_123 = document.querySelector('.customInputContainer_123');

            window.addEventListener('click', (e) => {
                if (document.querySelector('.searchInput_123').contains(e.target)) {
                    document.querySelector('.searchInput_123').classList.add('focus');
                } else {
                    document.querySelector('.searchInput_123').classList.remove('focus');
                }
            });





            function attachClickEventToLiElements() {
                ul_123.querySelectorAll('li').forEach(li => {
                    li.onclick = function () {
                        updateData123(this);
                    };
                });
            }

            document.getElementById('searchInput_123').addEventListener('input', function () {
                var key = this.value;
                $.get('demoFind', {key: key}, function (html) {
                    var $newListMessage2 = $(html);
                    $('#loadfindTransfer').html($newListMessage2.find('#loadfindTransfer').html());
                    // G?n l?i s? ki?n onclick cho c�c ph?n t? li m?i
                    attachClickEventToLiElements();
                }).fail(function (error) {
                    console.error('Error:', error);
                });
            });



            customInput_123.addEventListener('click', () => {
                customInputContainer_123.classList.toggle('show');
            });




            function 123(data) {

                let selectedBankName = data.innerText;
                let selectedBankImageSrc = data.querySelector('img').src;


                //    selectedData.innerHTML = `<img style="width:30px" src="${selectedBankImageSrc}" alt="${selectedBankName}" style="width:50px; height:auto; margin-right:10px;">${selectedBankName}`;
                selectedData_123.innerHTML = '<img style="width:30px" src="' + selectedBankImageSrc + '" alt="' + selectedBankName + '" style="width:50px; height:auto; margin-right:10px;">' + selectedBankName;

                let bankInput = document.querySelector('input[name="findUser"]');
                bankInput.value = selectedBankName.trim();


                console.log('Updated input value:', bankInput.value);


                document.querySelectorAll('.options_123 ul li').forEach(li => {
                    li.classList.remove('selected');
                });
                data.classList.add('selected');


                customInputContainer_123.classList.remove('show');
            };


            ul_123.querySelectorAll('li').forEach(li => {
                li.onclick = function () {
                    updateData(this);
                };
            });



        </script>
    </body>





</html>