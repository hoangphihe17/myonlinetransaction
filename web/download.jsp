<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="style.css">
        <title>Download Button</title>
        <!-- S-Tech04 -->
        <style>
           
            .body_download
            {
                display: flex;
                justify-content: center;
                align-items: center;
                text-align: center;
                background: #222;

                width: fit-content;
                border-radius: 52px;
            }
            .btn_dl
            {
                position: relative;
                width: 200px;
                height: 70px;
                background: #444;
                color: #999;
                font-size: 20px;
                font-weight: 700;
                border-radius: 50px;
                cursor: pointer;
                text-decoration: none;
                transition: 0.8s;
                transition-delay: 0.5s;
                overflow: hidden;
            }
            .btn_dl .fas
            {
                margin-right: 8px;
            }
            .btn_dl .download
            {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                white-space: nowrap;
                transition: 0.3s;
            }
            .btn_dl.active .download
            {
                transform: translate(-50%,60px);
                animation: bottom_top 0.5s linear forwards;
                animation-delay: 12s;
            }
            .btn_dl .completed_dl
            {
                position: absolute;
                top: 0%;
                left: 50%;
                transform: translate(-50%,-60px);
                white-space: nowrap;
                color: #222;
            }
            .btn_dl.active .completed_dl
            {
                animation: top_bottom 1.3s linear forwards;
                animation-delay: 6.5s;
            }
            .btn_dl.active
            {
                width: 600px;
                height: 15px;
                animation: btn_width 4.5s linear forwards;
                animation-delay: 5s;
            }
            .btn_dl.active::before
            {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 0;
                height: 100%;
                background: #5c5cff;
                animation: before_active 10s linear forwards;
                animation-delay: 1.5s;
            }
            .btn_dl.active::after
            {
                content: '\f00c';
                position: absolute;
                top: 50%;
                left: 50%;
                font-family: "Font Awesome 5 Free";
                color: #222;
                transform: translate(-50%,-50%) scale(0);
                transition: 0.3s;
                animation: check_scale 2s linear forwards;
                animation-delay: 8.5s;
            }
            @keyframes before_active
            {
                0%
                {
                    width: 0%;
                    height: 100%;
                }
                30%,95%
                {
                    width: 100%;
                    height: 100%;
                }
                100%
                {
                    width: 100%;
                    height: 0%;
                }
            }
            @keyframes btn_width
            {
                0%
                {
                    width: 600px;
                    height: 15px;
                }
                20%,70%
                {
                    width: 200px;
                    height: 70px;
                }
                80%,90%
                {
                    width: 100px;
                    height: 100px;
                }
                100%
                {
                    width: 200px;
                    height: 70px;
                }
            }
            @keyframes top_bottom
            {
                0%
                {
                    transform: translate(-50%,-60px);
                }
                20%,90%
                {
                    top: 50%;
                    transform: translate(-50%,-50%);
                }
                100%
                {
                    top: 120%;
                    transform: translate(-50%,-50%);
                }
            }
            @keyframes bottom_top
            {
                0%
                {
                    transform: translate(-50%,60px);
                }
                100%
                {
                    transform: translate(-50%,-50%);
                }
            }
            @keyframes check_scale
            {
                0%
                {
                    transform: translate(-50%,-50%) scale(0);
                }
                20%,90%
                {
                    transform: translate(-50%,-50%) scale(1.3);
                }
                100%
                {
                    transform: translate(-50%,100px) scale(1.3);
                }
            }
        </style>
    </head>
    <body>
      
    <div class="body_download">
        <button id="downloadButton" class="btn_dl">
            <div class="completed_dl">
                <i class="fas fa-check-circle"></i>
                <span>Completed</span>
            </div>
            <div class="download">
                <i class="fas fa-download"></i>
                <span>Download</span>
            </div>
        </button>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const downloadButton = document.getElementById('downloadButton');
            downloadButton.addEventListener('click', function () {
                downloadButton.classList.add('active');
                setTimeout(() => {
                    downloadButton.classList.remove('active');
                }, 13000);
                
                // Thực hiện AJAX request
                const xhr = new XMLHttpRequest();
                xhr.open('GET', 'excel?id=${sessionScope.id}&page=${sessionScope.page}', true);
                xhr.responseType = 'blob';
                xhr.onload = function () {
                    if (xhr.status === 200) {
                        const blob = xhr.response;
                        const url = window.URL.createObjectURL(blob);
                        const a = document.createElement('a');
                        a.style.display = 'none';
                        a.href = url;
                        a.download = 'file.xlsx';
                        document.body.appendChild(a);
                        a.click();
                        window.URL.revokeObjectURL(url);
                    }
                };
                xhr.send();
            });
        });
    </script>


</html>