
document.addEventListener('DOMContentLoaded', function () {
    
  

    document.querySelectorAll('.slider_post').forEach((slider, index) => {
        let currentIndex = 0; // Khởi tạo chỉ số hiện tại cho mỗi slider

        const slides = slider.querySelectorAll('.slide_post');
        const counter = slider.querySelector('.counter_post');
        const total = slides.length;
        const updateCounter = () => {
            counter.textContent = `${currentIndex + 1}/${total}`;
        };

        const showSlide = (index) => {
            if (index >= total)
                currentIndex = 0;
            if (index < 0)
                currentIndex = total - 1;
            slider.querySelector('.slides_post').style.transform = `translateX(${-currentIndex * 100}%)`;
            updateCounter();
        };

        slider.querySelector('#next_post').addEventListener('click', () => {
            currentIndex++;
            showSlide(currentIndex);
        });

        slider.querySelector('#prev_post').addEventListener('click', () => {
            currentIndex--;
            showSlide(currentIndex);
        });

        updateCounter(); // Cập nhật bộ đếm khi trang tải xong cho mỗi slider
    });




});



















