


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .chat-box_sock {
                width: 300px;
                height: 400px;
                border: 1px solid #ddd;
                padding: 10px;
                overflow-y: auto;
                background-color: #f9f9f9;
                position: fixed;
                bottom: 10px;
                right: 10px;
            }
            .message-input {
                width: calc(100% - 20px);
                padding: 10px;
                margin-top: 10px;
            }
            .message_sock {
                padding: 5px;
                margin-bottom: 10px;
                border-radius: 5px;
                background-color: #fff;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
            }
            .floating-message {
                position: fixed;
                top: 50px;
                right: 100%; /* Start off-screen */
                white-space: nowrap;
                opacity: 1; /* Make sure text is visible */
                z-index: 99999999;
            }
            @keyframes floatMessage {
                0% {
                    left: 100%;
                }
                100% {
                    left: -100%;
                } /* Ensure text moves from right to left */
            }
            .floating-message{
                width: fit-content;
                padding: 10px;
                background-color: gray;
                border: 2px solid green;
                border-radius: 14px;
            }

            .message-input {
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 5px;
                font-size: 16px;
                width: 70%; 
                margin-right: 10px; 
            }

            #sendButton {
                padding: 10px;
                background-color: #01C854;
                border: none;
                border-radius: 5px;
                color: #fff;
                font-size: 16px;
                cursor: pointer;
            }

            #sendButton:hover {
                background-color: #008033; /* M�u khi di chu?t qua n�t */
            }


            .chat-container_sock
            {
                position: fixed;
                bottom: 0;
                right: 0;
                margin: 20px;
            }

            .chat-toggle-sock {
/*                background-color: #01C854;*/
                border: none;
                border-radius: 50%;
                cursor: pointer;
                font-size: 24px;
                padding: 10px;
                color: white;
            }

            .chat-box_sock {
                display: none; /* ?n chat box m?c ??nh */
                background-color: white;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
                overflow: hidden;
                max-width: 300px;
                bottom: 175px;
                right: 60px;
                color: var(--color-dark);
    background-color: var(--color-light);
            }
            .message-history {
                 color: var(--color-dark);
    background-color: var(--color-light);
                max-height: 315px;
                overflow-y: auto;
                                    min-height: 288px;

            }

            .message_sock {
                color: var(--color-dark);
    background-color: var(--color-light);
                display: flex;
                gap: 5px;
                padding: 10px;
                align-items: center;
            }

            .message-avatar {
                width: 30px;
                height: 30px;
                border-radius: 50%;
            }

            .message-content {
                background-color: #f0f0f0;
                padding: 10px;
                border-radius: 10px;
                max-width: 70%;
            }

            .chat-form {
                display: flex;
                gap: 10px;

                background-color: #f8f8f8;
                align-items: center;
            }

            .message-input {
                flex-grow: 1;
                padding: 10px;
                border: 2px solid #ddd;
                border-radius: 20px;
                outline: none;
            }

            #sendButton {
                padding: 5px 15px;
                background-color: #01C854;
                border: none;
                border-radius: 20px;
                color: white;
                cursor: pointer;
            }

        </style>
    </head>
    <body>

 

        <div style="z-index: 9999999" class="chat-container_sock">

            <!--<button >?</button>  N�t ?? m? form chat -->
            <img style="    width: 60px;
    position: absolute;
    /* padding: 34px; */
    bottom: 112px;
    right: 8px;" id="toggleChat_sock" class="chat-toggle-sock" src="image/earth.jpg">
            <div class="chat-box_sock" id="chatBox_sock">
                <div  style=" font-family: inherit; font-weight: bolder;text-align: center;color:gray ;font-size: 25px"> Chat community</div>
                <div id="messageHistory" class="message-history">
                    <!-- S? d?ng JSTL ?? render c�c tin nh?n -->
                    <c:if test="${list != null}">
                        <c:forEach var="m" items="${list}">
                            <div class="message_sock">
                                <img src="${m.accountid.avatar}" class="message-avatar">
                                <span class="message-content">${m.message}</span>
                            </div>
                        </c:forEach>
                    </c:if>
                </div>
                <form style="color: var(--color-dark);
    background-color: var(--color-light);" id="chatForm" method="post" action="chatCommunity" class="chat-form">
                    <input hidden class="userName" value="@${sessionScope.account.userName}">
                    <input  type="text" name="chatMessage" class="message-input" id="chatInput" placeholder="Type your message here..." autofocus>
                    <input type="submit" id="sendButton" value="Send"> 
                </form>
            </div>
        </div>


        <script>

            document.getElementById('toggleChat_sock').addEventListener('click', function () {
                var chatBox = document.getElementById('chatBox_sock');
                if (chatBox.style.display === 'none') {
                    chatBox.style.display = 'block';
                    document.getElementById('messageHistory').scrollTop = document.getElementById('messageHistory').scrollHeight;

                } else {
                    chatBox.style.display = 'none';
                }
            });


            document.getElementById('chatForm').addEventListener('submit', function (event) {
                event.preventDefault(); 
                const formData = new FormData(this); 
                const xhr = new XMLHttpRequest();
                xhr.open('POST', 'chatCommunity', true);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.onload = function () {
                    if (xhr.status === 200) {


                     
                        const newMessage = document.createElement('div');
                        newMessage.classList.add('message_sock');
                        newMessage.innerText = message;
                        document.getElementById('messageHistory').scrollTop = document.getElementById('messageHistory').scrollHeight;
                        document.getElementById('chatInput').value = '';
                    } else {
                        console.error('An error occurred:', xhr.statusText);
                    }
                };
                xhr.onerror = function () {
                    console.error('Request failed');
                };
                xhr.send(new URLSearchParams(formData).toString());
            });



            var ws;
            var messageQueue = [];
            var isMessageShowing = false;

            function initWebSocket() {
                if ("WebSocket" in window) {
                    ws = new WebSocket("ws://localhost:9999/swp391/chat");

                    ws.onopen = function () {
                        console.log("Connected to the WebSocket server");
                    };

                    ws.onmessage = function (evt) {
                        var received_msg = evt.data;
                        messageQueue.push(received_msg); // Push the received message to the queue
                        displayNextMessage(); // Try to display the next message
                        addToMessageHistory(received_msg); // Add the message to the chat history
                    };

                    ws.onclose = function () {
                        console.log("Connection to WebSocket server closed");
                    };

                    ws.onerror = function (err) {
                        console.error("WebSocket encountered error: ", err.message, "Closing socket");
                        ws.close();
                    };
                } else {
                    alert("WebSocket NOT supported by your Browser!");
                }
            }

            function sendMessage() {
                var input = document.getElementById('chatInput');
                var message = input.value.trim();
                var userName = document.querySelector(".userName").value.trim();
                if (message) {
                    var fullMessage = userName + ": " + message;
                    ws.send(fullMessage); // Send the message through WebSocket
                    input.value = ''; // Clear the input box after sending
                }
            }

            function displayMessage(message) {
                // Add message to floating queue
                messageQueue.push(message);
                displayNextMessage(); // Display next floating message if possible
            }

            function displayNextMessage() {
                if (!isMessageShowing && messageQueue.length > 0) {
                    isMessageShowing = true; // Mark that we are showing a message
                    var message = messageQueue.shift(); // Remove the first message from the queue

                    var floatingMessage = document.createElement('div');
                    floatingMessage.classList.add('floating-message');
                    floatingMessage.innerText = message;
                    document.body.appendChild(floatingMessage);
                    floatingMessage.style.animation = 'floatMessage 20s linear forwards';

                    floatingMessage.addEventListener('animationend', () => {
                        document.body.removeChild(floatingMessage);
                        isMessageShowing = false; // Mark that we are no longer showing a message
                        displayNextMessage(); // Display the next message
                    });
                }
            }

            function addToMessageHistory(message) {
                var messageElement = document.createElement('div');
                messageElement.classList.add('message_sock');
                messageElement.innerHTML = '<div style="display: flex; gap: 5px;">' + message + '</div>';
                var messageHistory = document.getElementById('messageHistory');
                messageHistory.appendChild(messageElement);
                messageHistory.scrollTop = messageHistory.scrollHeight; // Scroll to the latest message
            }

            document.getElementById('chatForm').addEventListener('submit', function (event) {
                event.preventDefault(); // Prevent default form submission
                sendMessage();
            });

            initWebSocket();



        </script>







    </body>
</html>
