<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">


        <title>profile with contact information - Bootdey.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                background: #eee;
                margin-top: 20px;
                /*            width: 100%;*/
            }

            .page-inner.no-page-title {
                padding-top: 30px;
            }

            .page-inner {
                position: relative;
                min-height: calc(100% - 56px);
                padding: 20px 30px 40px 30px;
                background: #f3f4f7;
            }

            .card.card-white {
                background-color: #fff;
                border: 1px solid transparent;
                border-radius: 4px;
                box-shadow: 0 0.05rem 0.01rem rgba(75, 75, 90, 0.075);
                padding: 25px;
            }

            .grid-margin {
                margin-bottom: 2rem;
            }

            .profile-timeline ul li .timeline-item-header {
                width: 100%;
                overflow: hidden;
            }

            .profile-timeline ul li .timeline-item-header img {
                width: 40px;
                height: 40px;
                float: left;
                margin-right: 10px;
                border-radius: 50%;
            }

            .profile-timeline ul li .timeline-item-header p {
                margin: 0;
                color: #000;
                font-weight: 500;
            }

            .profile-timeline ul li .timeline-item-header p span {
                margin: 0;
                color: #8e8e8e;
                font-weight: normal;
            }

            .profile-timeline ul li .timeline-item-header small {
                margin: 0;
                color: #8e8e8e;
            }

            .profile-timeline ul li .timeline-item-post {
                padding: 20px 0 0 0;
                position: relative;
            }

            .profile-timeline ul li .timeline-item-post>img {
                width: 100%;
            }

            .timeline-options {
                overflow: hidden;
                margin-top: 20px;
                margin-bottom: 20px;
                border-bottom: 1px solid #f1f1f1;
                padding: 10px 0 10px 0;
            }

            .timeline-options a {
                display: block;
                margin-right: 20px;
                float: left;
                color: #2b2b2b;
                text-decoration: none;
            }

            .timeline-options a i {
                margin-right: 3px;
            }

            .timeline-options a:hover {
                color: #5369f8;
            }

            .timeline-comment {
                overflow: hidden;
                margin-bottom: 10px;
                width: 100%;
                border-bottom: 1px solid #f1f1f1;
                padding-bottom: 5px;
            }

            .timeline-comment .timeline-comment-header {
                overflow: hidden;
            }

            .timeline-comment .timeline-comment-header img {
                width: 30px;
                border-radius: 50%;
                float: left;
                margin-right: 10px;
            }

            .timeline-comment .timeline-comment-header p {
                color: #000;
                float: left;
                margin: 0;
                font-weight: 500;
            }

            .timeline-comment .timeline-comment-header small {
                font-weight: normal;
                color: #8e8e8e;
            }

            .timeline-comment p.timeline-comment-text {
                display: block;
                color: #2b2b2b;
                font-size: 14px;
                padding-left: 40px;
            }

            .post-options {
                overflow: hidden;
                margin-top: 15px;
                margin-left: 15px;
            }

            .post-options a {
                display: block;
                margin-top: 5px;
                margin-right: 20px;
                float: left;
                color: #2b2b2b;
                text-decoration: none;
                font-size: 16px !important;
            }

            .post-options a:hover {
                color: #5369f8;
            }

            .online {
                position: absolute;
                top: 2px;
                right: 2px;
                display: block;
                width: 9px;
                height: 9px;
                border-radius: 50%;
                background: #ccc;
            }

            .online.on {
                background: #2ec5d3;
            }

            .online.off {
                background: #ec5e69;
            }

            #cd-timeline::before {
                border: 0;
                background: #f1f1f1;
            }

            .cd-timeline-content p,
            .cd-timeline-content .cd-read-more,
            .cd-timeline-content .cd-date {
                font-size: 14px;
            }

            .cd-timeline-img.cd-success {
                background: #2ec5d3;
            }

            .cd-timeline-img.cd-danger {
                background: #ec5e69;
            }

            .cd-timeline-img.cd-info {
                background: #5893df;
            }

            .cd-timeline-img.cd-warning {
                background: #f1c205;
            }

            .cd-timeline-img.cd-primary {
                background: #9f7ce1;
            }

            .page-inner.full-page {
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
            }

            .user-profile-card {
                text-align: center;
            }

            .user-profile-image {
                width: 100px;
                height: 100px;
                margin-bottom: 10px;
            }

            .team .team-member {
                display: block;
                overflow: hidden;
                margin-bottom: 10px;
                float: left;
                position: relative;
            }

            .team .team-member .online {
                top: 5px;
                right: 5px;
            }

            .team .team-member img {
                width: 40px;
                float: left;
                border-radius: 50%;
                margin: 0 5px 0 5px;
            }

            .label.label-success {
                background: #43d39e;
            }

            .label {
                font-weight: 400;
                padding: 4px 8px;
                font-size: 11px;
                display: inline-block;
                line-height: 1;
                color: #fff;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: 0.25em;
            }

            body {
                font-family: Arial;
                margin: 0 auto; /* Center website */
                padding: 20px;
            }

            .heading {
                font-size: 25px;
                margin-right: 25px;
            }

            .fa {
                font-size: 25px;
            }

            .checked {
                color: orange;
            }

            /* Three column layout */
            .side {
                float: left;
                width: 15%;
                margin-top:10px;
            }

            .middle_star {
                margin-top:10px;
                float: left;
                width: 70%;
            }

            /* Place text to the right */
            .right {
                text-align: right;
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* The bar container */
            .bar-container {
                width: 100%;
                background-color: #f1f1f1;
                text-align: center;
                color: white;
            }

            /* Individual bars */
            .bar-5 {
                width: 60%;
                height: 18px;
                background-color: #04AA6D;
            }
            .bar-4 {
                width: 30%;
                height: 18px;
                background-color: #2196F3;
            }
            .bar-3 {
                width: 10%;
                height: 18px;
                background-color: #00bcd4;
            }
            .bar-2 {
                width: 4%;
                height: 18px;
                background-color: #ff9800;
            }
            .bar-1 {
                width: 15%;
                height: 18px;
                background-color: #f44336;
            }
            h3{
                font-size: larger;
                margin: 0px;
            }
            p{
                margin: 1px;
            }

            .upload_input11{
                position: absolute;
                width: 30px;
                position: absolute;
                width: 30px;
                left: 138px;
                top: 150px;
                border-radius: 50%;
                border: 1px dashed green;
            }

            .fa-star-half {
                position: absolute; /* Absolute positioning to overlap the full star */
                color: gold; /* Color of the star */
                overflow: hidden; /* Hide the overflow */
                width: 50%; /* Show only half of the star */
            }
            .notifications_ok li{
                opacity: 1;
                z-index: 99999;
            }
        </style>
    </head>

    <body>
        <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />-->
        <ul style="z-index: 999;" class="notifications_ok">
           
        </ul>
 
            
        <div style="width: 100% ;display: flex" class="container">



            <c:set var="c" value="${infoUser}"></c:set>
                <div   class="col-lg-5 col-xl-3">
                    <div  class="card card-white grid-margin">
                        <div class="card-heading clearfix">
                            <h4 style="text-align: center" class="card-title">User Profile</h4>
                        </div>
                        <form id="formAVT" action="updateProfile" method="post" enctype="multipart/form-data">

                            <div class="card-body user-profile-card mb-3">
                                <img id="avt_user" style="margin: 0 auto" src="${c.avatar}"

                                 class="user-profile-image rounded-circle" alt />
                            <c:if test="${sessionScope.account!=null && sessionScope.account.accountid==requestScope.id}">
                                                            <img class="upload_input11" src="image/camera.jpg">

                            </c:if>
                            
                            <input hidden type="file" name="messageImage" class="visually-hidden" id="upload_input11" >



                            <h4 style="border-bottom: 1px solid white;font-weight: bolder; color:silver" class="text-center h6 mt-2">@${c.userName}</h4>
                            <p style="margin: 0px" class="text-center small">Date : ${c.createAt}</p>

                        </div>
                    </form>

                    <hr style="margin: 0" />
                    <div class="card-heading clearfix mt-3">
                        <h4 style="text-align: center;
                            margin: 0;" class="card-title">User Rating</h4>
                    </div>
                    <div class="card-body mb-3">



                        <div style="width: 200px;
                             margin-left: -19px;text-align: center;" class="reviews">



                            <c:if test="${avg==null}">
                                <div style="display: flex ;margin-left: 34px;">

                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                </div>

                            </c:if>
                            <c:if test="${avg!=null}">
                                <div style="display: flex ;margin-left: 34px;">

                                    <c:forEach begin="0" end="${avg-1}">
                                        <span class="fa fa-star checked"></span>

                                    </c:forEach>

                                    <c:forEach begin="${avg}" end="4">
                                        <span class="fa fa-star "></span>


                                    </c:forEach>
                                </div>

                            </c:if>


                            <!--                            <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star "></span>-->



                            <p> 

                                <c:if test="${avg==null}">
                                    0
                                </c:if>
                                <c:if test="${avg!=null}">
                                    ${avg}
                                </c:if>

                                average based on 
                                <c:if test="${number==null}">
                                    0
                                </c:if>
                                <c:if test="${number!=null}">
                                    ${number}
                                </c:if>
                                reviews.</p>
                            <hr style="border:3px solid #f1f1f1">

                            <div class="row">
                                <div class="side">
                                    <div style="display: flex">5  <span style="font-size: large;" class="fa fa-star checked"></span></div>
                                </div>
                                <div class="middle_star">
                                    <div class="bar-container">
                                        <div class="bar-5"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>

                                        <c:if test="${star.get(4).revenue==null}">
                                            0
                                        </c:if>
                                        <c:if test="${star.get(4).revenue!=null}">
                                            ${star.get(4).revenue}
                                        </c:if>

                                    </div>

                                </div>


                                <div class="side">
                                    <div>4  <span style="font-size: large;" class="fa fa-star checked"></span></div>
                                </div>
                                <div class="middle_star">
                                    <div class="bar-container">
                                        <div class="bar-4"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>
                                        <c:if test="${star.get(3).revenue==null}">
                                            0
                                        </c:if>
                                        <c:if test="${star.get(3).revenue!=null}">
                                            ${star.get(3).revenue}
                                        </c:if>
                                    </div>
                                </div>


                                <div class="side">
                                    <div>3  <span style="font-size: large;" class="fa fa-star checked"></span></div>
                                </div>
                                <div class="middle_star">
                                    <div class="bar-container">
                                        <div class="bar-3"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>
                                        <c:if test="${star.get(2).revenue==null}">
                                            0
                                        </c:if>
                                        <c:if test="${star.get(2).revenue!=null}">
                                            ${star.get(2).revenue}
                                        </c:if>
                                    </div>
                                </div>


                                <div class="side">
                                    <div>2  <span style="font-size: large;" class="fa fa-star checked"></span></div>
                                </div>
                                <div class="middle_star">
                                    <div class="bar-container">
                                        <div class="bar-2"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>
                                        <c:if test="${star.get(1).revenue==null}">
                                            0
                                        </c:if>
                                        <c:if test="${star.get(1).revenue!=null}">
                                            ${star.get(1).revenue}
                                        </c:if>
                                    </div>
                                </div>


                                <div class="side">
                                    <div>1  <span style="font-size: large;" class="fa fa-star checked"></span></div>
                                </div>
                                <div class="middle_star">
                                    <div class="bar-container">
                                        <div class="bar-1"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>
                                        <c:if test="${star.get(0).revenue==null}">
                                            0
                                        </c:if>
                                        <c:if test="${star.get(0).revenue!=null}">
                                            ${star.get(0).revenue}
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <hr />
                    <div class="card-heading clearfix mt-3">
                        <h4 class="card-title">About</h4>
                    </div>
                    <div class="card-body mb-3">
                        <p class="mb-0">Lorem ipsum dolor sitelt amet, consectetur adipis icing elit, sed do
                            eiusmod tempor incididunt utitily labore et dolore magna aliqua metavta.</p>
                    </div>
                    <hr />
                    <div class="card-heading clearfix mt-3">
                        <h4 class="card-title">Contact Information</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless mb-0 text-muted">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email:</th>
                                        <td>${c.email}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone:</th>
                                        <td>(+84) ${c.phone}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address:</th>
                                        <td>VietNam (+7 GMT)</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div id="container_123" class="col-lg-7 col-xl-6">



                <jsp:include page="feedsProfile.jsp"></jsp:include>


                </div>


            </div>
                
       <script src="https://cdn.ckeditor.com/ckeditor5/41.0.0/super-build/ckeditor.js"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.6.3.js"
    integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="./javacript1/index2.js"></script>
    <script src="./javacript1/index1.js"></script>

    <script src="./javacript1/index.js"></script>
            <script type="text/javascript">




                document.addEventListener("DOMContentLoaded", function () {
                    changeBarWidths('bar-5', ['${single.get(4)}%']);
                    changeBarWidths('bar-4', ['${single.get(3)}%']);
                    changeBarWidths('bar-3', ['${single.get(2)}%']);
                    changeBarWidths('bar-2', ['${single.get(1)}%']);
                    changeBarWidths('bar-1', ['${single.get(0)}%']);

                });


                document.addEventListener("DOMContentLoaded", function () {
                    // Step 1: Trigger file input click
                    document.querySelector('.upload_input11').addEventListener('click', function () {
                        document.getElementById('upload_input11').click();
                    });

                    // Step 2: File type validation
                    document.getElementById('upload_input11').addEventListener('change', function (e) {
                        const file = e.target.files[0];
                        if (file) {
                            const fileType = file.type;
                            const validImageTypes = ['image/jpeg', 'image/jpg', 'image/png'];
                            if (!validImageTypes.includes(fileType)) {
                                alert('File is not an image. Please select a JPEG, JPG, or PNG file.');
                            } else {
                                // Step 3: Form submission
                                const formData = new FormData();
                                formData.append('messageImage', file);
                                // Add other form fields if necessary
                                fetch('updateProfile', {
                                    method: 'POST',
                                    body: formData
                                }).then(response => {
                                    if (response.ok) {
                                        return response.json();
                                    }
                                    throw new Error('Network response was not ok.');
                                }).then(data => {
                                    console.log(data);

                                }).catch(error => {
                                    console.error('There has been a problem with your fetch operation:', error);
                                });
                            }
                        }
                    });

                    // Prevent form from submitting normally to avoid page reload
                    document.querySelector('form').addEventListener('submit', function (e) {
                        e.preventDefault();
                    });
                });


                function changeBarWidths(barClass, widths) {
                    const bars = document.querySelectorAll('.' + barClass);

                    if (!bars.length || widths.length !== bars.length) {
                        console.error('Kh�ng t�m th?y ho?c s? l??ng gi� tr? chi?u r?ng kh�ng kh?p v?i s? l??ng thanh.');
                        return;
                    }

                    bars.forEach((bar, index) => {
                        bar.style.width = widths[index];
                    });
                }


      document.getElementById('viewCommentsBtn').addEventListener('click', function (event) {
                                        // Prevent the default form submission
                                        event.preventDefault();

                                        // Submit the form programmatically
                                        document.getElementById('postDetailForm').submit();

                                        // Optionally, you can close the current tab after opening the new one
                                        // window.close();
                                    });








                                    var page = 1;
                                    var loading = false;



                                    $(document).ready(function () {
                                        // S? d?ng s? ki?n scroll ?? ki?m tra v� t?i th�m d? li?u khi c?n thi?t
                                        $(window).scroll(function () {
                                            if (!loading && $(window).scrollTop() + $(window).height() >= $(document).height() - 8) {
                                                page++;
                                                loadMoreProducts(page);
                                            }
                                        });

                                        // G?n s? ki?n click cho t?t c? c�c ph?n t? .feed hi?n t?i v� t??ng lai
                                        // b?ng c�ch s? d?ng event delegation tr�n .feeds
                                        $('.feeds').on('click', '.feed', function () {
//                                            ============================


//                                          =====================================
                                        });
                                    });

                                    function loadMoreProducts(page) {
                                        loading = true;
                                        $.ajax({
                                            url: 'profileUser?page=' + page,
                                            type: 'GET',
                                            success: function (response) {
                                                var newContent = $(response).find('.feeds').html();
                                                if (newContent.trim() !== "") {
                                                    $('.feeds').append(newContent);
                                                } else {
                                                    // G? b? tr�nh x? l� s? ki?n scroll khi kh�ng c�n d? li?u m?i
                                                    $(window).off('scroll'); // G? b? s? ki?n scroll
                                                }
                                                loading = false;
                                            },
                                            error: function (xhr, status, error) {
                                                console.error('Error:', error);
                                                loading = false;
                                            }
                                        });
                                    }







                                    document.querySelectorAll('.caption').forEach(caption => {
                                        const toggleBtn = caption.querySelector('.toggle-btn_caption');
                                        const captionPost = caption.querySelector('.caption_post');

                                        toggleBtn.addEventListener('click', function () {
                                            const isExpanded = captionPost.classList.contains('expanded');
                                            if (isExpanded) {
                                                captionPost.classList.remove('expanded');
                                                toggleBtn.textContent = 'Show More';
                                            } else {
                                                captionPost.classList.add('expanded');
                                                toggleBtn.textContent = 'Show Less';
                                            }
                                        });
                                    });



                                    document.addEventListener("DOMContentLoaded", function () {
                                        // ??ng k� s? ki?n click cho c�c n�t hi?n th? modal
                                        document.querySelectorAll(".view_modal_shareP").forEach(function (viewBtn) {

                                            viewBtn.addEventListener('click', function () {
                                                const popup = viewBtn.closest('.popup123'); // T�m popup g?n nh?t t??ng ?ng v?i n�t ???c click
                                                event.stopPropagation();
                                                popup.style.display = 'block';
                                            });
                                        });

                                        // ??ng k� s? ki?n click cho c�c n�t ?�ng modal
                                        document.querySelectorAll(".popup123 .close").forEach(function (closeBtn) {
                                            closeBtn.addEventListener('click', function () {
                                                const popup = closeBtn.closest('.popup123'); // T�m popup g?n nh?t t??ng ?ng v?i n�t ???c click
                                                popup.style.display = 'none';

                                            });
                                        });

                                        // ??ng k� s? ki?n click cho n�t sao ch�p
                                        document.querySelectorAll(".popup123 .field button").forEach(function (copyBtn) {
                                            copyBtn.addEventListener('click', function () {
                                                const input = copyBtn.previousElementSibling; // Input ngay tr??c n�t copy
                                                input.select();
                                                if (document.execCommand("copy")) {
                                                    const field = copyBtn.parentElement;
                                                    field.classList.add("active");
                                                    copyBtn.innerText = "Copied";
                                                    setTimeout(() => {
                                                        window.getSelection().removeAllRanges();
                                                        field.classList.remove("active");
                                                        copyBtn.innerText = "Copy";
                                                    }, 3000);
                                                }
                                            });
                                        });

                                        // ??ng k� s? ki?n cho c�c n�t chia s? tr�n m?ng x� h?i
                                        document.querySelectorAll('.shareBtn_product').forEach(function (button) {
                                            button.addEventListener('click', function (event) {
                                                shareOnSocial(event, button);
                                            });
                                        });

                                        function shareOnSocial(event, button) {
                                            const postUrl = button.getAttribute('data-url');
                                            const socialNetwork = button.getAttribute('data-social');
                                            let shareUrl;

                                            switch (socialNetwork) {
                                                case 'facebook':
                                                    shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'twitter':
                                                    shareUrl = 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'instagram':
                                                    alert('Instagram kh�ng h? tr? chia s? tr?c ti?p t? trang web. Vui l�ng s? d?ng ?ng d?ng di ??ng.');
                                                    return;
                                                case 'line':
                                                    shareUrl = 'https://lineit.line.me/share/ui?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                case 'telegram':
                                                    shareUrl = 'https://telegram.me/share/url?url=' + encodeURIComponent(postUrl);
                                                    break;
                                                default:
                                                    shareUrl = '';
                                            }

                                            if (shareUrl !== '') {
                                                window.open(shareUrl, socialNetwork + '-share-dialog', 'width=626,height=436');
                                            }
                                        }
                                    });















                                    document.querySelectorAll('.price-input').forEach(function (element) {
                                        element.addEventListener('input', function (e) {
                                            var input = e.target.value;
                                            var numericInput = input.replace(/[^\d]/g, '');



                                            var formattedInput = numericInput
                                                    .split('')
                                                    .reverse()
                                                    .join('')
                                                    .replace(/\d{3}(?=\d)/g, '$&,')
                                                    .split('')
                                                    .reverse()
                                                    .join('');

                                            e.target.value = formattedInput;
                                        });
                                    });



                                    document.getElementById('numberInput_Price').addEventListener('input', function (e) {
                                        var input = e.target.value;
                                        // Lo?i b? t?t c? k� t? kh�ng ph?i s? v� d?u ph?y
                                        var numericInput = input.replace(/[^\d]/g, '');

                                        // Ki?m tra n?u c� k� t? kh�ng ph?i s? ?� b? lo?i b?
                                        if (numericInput === '' && input !== '' && input.replace(/,/g, '') !== '') {
                                            document.getElementById('errorMessage_Price').style.display = 'block';
                                        } else {
                                            document.getElementById('errorMessage_Price').style.display = 'none';
                                        }

                                        // ??nh d?ng l?i s? v?i d?u ph?y sau m?i 3 ch? s? t? ph?i sang tr�i
                                        var formattedInput = numericInput
                                                .split('') // Chuy?n ??i chu?i th�nh m?ng c�c k� t?
                                                .reverse() // ??o ng??c m?ng ?? d? d�ng th�m d?u ph?y
                                                .join('') // Chuy?n ??i m?ng ng??c l?i th�nh chu?i
                                                .replace(/\d{3}(?=\d)/g, '$&,') // Th�m d?u ph?y sau m?i 3 s?
                                                .split('') // Chuy?n ??i chu?i th�nh m?ng l?i l?n n?a
                                                .reverse() // ??o ng??c m?ng ?? tr? l?i tr?ng th�i ban ??u
                                                .join(''); // Chuy?n ??i m?ng th�nh chu?i

                                        e.target.value = formattedInput;
                                    });
















                                    const notifications_ok = document.querySelector(".notifications_ok");

                                    const toastDetails = {
                                        timer: 3000,
                                        success: {
                                            icon: 'fa-circle-check',
                                            text: 'Success: ' // C?p nh?t th�nh c�ng
                                        },
                                        error: {
                                            icon: 'fa-circle-xmark',
                                            text: 'Error: ' // C?p nh?t c� l?i
                                        },
                                        warning: {
                                            icon: 'fa-triangle-exclamation',
                                            text: 'Warning: ' // C?p nh?t c?nh b�o
                                        },
                                        info: {
                                            icon: 'fa-circle-info',
                                            text: 'Info: ' // C?p nh?t th�ng tin
                                        }
                                    };

                                    const removeToast = (toast) => {
                                        toast.classList.add("hide");
                                        if (toast.timeoutId)
                                            clearTimeout(toast.timeoutId);
                                        setTimeout(() => toast.remove(), 500);
                                    };

// C?p nh?t h�m createToast ?? nh?n th�m m?t tham s? message
                                    const createToast_123 = (id, message) => {
                                        const {icon, text} = toastDetails[id];
                                        const toast = document.createElement("li");
                                        console.log(icon);
                                        console.log(message);
                                        console.log(text);


                                        toast.className = `toast ` + id;
                                        toast.innerHTML = '<div style="z-index:99999;opacity:1" class="column_toast">' +
                                                '<i class="fa-solid ' + icon + '"></i>' +
                                                '<span style="color: black">' + text + message + '</span>' + // Th�m message v�o ?�y
                                                '</div>' +
                                                '<i class="fa-solid fa-xmark" onclick="removeToast(this.parentElement)"></i>';

                                        notifications_ok.appendChild(toast);
                                        toast.timeoutId = setTimeout(() => removeToast(toast), toastDetails.timer);
                                    };










                                    const editButtons = document.querySelectorAll('.editPost');

// L?p qua t?ng n�t Edit v� th�m s? ki?n click
                                    editButtons.forEach(function (btn) {
                                        btn.addEventListener('click', function () {
                                            // T�m modal-overlay g?n nh?t t? n�t Edit ???c click
                                            const modal = btn.closest('.feed').querySelector('.modal-overlay');

                                            // Hi?n th? modal-overlay
                                            modal.classList.add('active');
                                            modal.style.display = 'block';

                                            // T�m n�t ?�ng trong modal hi?n t?i v� th�m s? ki?n click ?? ?�ng modal
                                            const close_Buttons = modal.querySelector('.close-btn');
                                            close_Buttons.addEventListener('click', function () {
                                                // ?�ng modal b?ng c�ch lo?i b? class 'active' v� ?n n�
                                                modal.classList.remove('active');
                                                modal.style.display = 'none';
                                            });
                                        });
                                    });









                                    // Function to update privacy indicator dynamically
                                    function updatePrivacyIndicator(listItem) {
                                        const feedContainer = listItem.closest('.feed');
                                        const iconElement = feedContainer.querySelector('.post_123 .privacy_123 i');
                                        const privacyText = feedContainer.querySelector('.post_123 .privacy_123 span');
                                        const activeItemIndex = Array.from(listItem.parentNode.children).indexOf(listItem);

                                        // Logic to update icon and text based on active item
                                        // This assumes the order of items reflects the desired privacy settings
                                        const privacySettings = [
                                            {icon: 'fa-globe', text: 'Public'},
                                            {icon: 'fa-user-friends', text: 'Friends'},
                                            {icon: 'fa-user', text: 'Specific'},
                                            {icon: 'fa-lock', text: 'Private'},
                                            {icon: 'fa-cog', text: 'Custom'}
                                        ];

                                        const setting = privacySettings[activeItemIndex];
                                        if (setting) {
                                            iconElement.className = 'fas ' + setting.icon; // Reset and apply new classes
                                            privacyText.innerText = setting.text;


                                            document.querySelectorAll('.helpme').forEach(function (item) {
                                                // T�m ph?n t? span ch?a text "Public" ho?c "Private"
                                                var spanText = item.querySelector('.public_Private_Span').innerText;

                                                // G�n gi� tr? c?a span v�o thu?c t�nh value c?a input c� t�n "uPublicPrivate"
                                                item.querySelector('input[name="uPublicPrivate"]').value = spanText;
                                            });
                                        }
                                    }








        </script>
    </body>

</html>