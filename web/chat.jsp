<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chat </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-/FUk7rGtJMmrQ3g6e3tJr8Xiv3L6q8cRcP4WOjfde7XW0IH/f5jvO3PyTzNsiFM+wePea6P1JYGzxaY9at6tGw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
              integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
        <style>



            body {
                font-family: 'Arial', sans-serif;
                background-color: #f4f7f6;
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
            }
            .chatbox-container {

                background-color: white;
                border-radius: 20px;
                box-shadow: 0 5px 15px rgba(0,0,0,0.2);
                overflow: hidden;
            }
            .chatbox-header {
                background-color: #5b36ac;
                color: white;
                padding: 15px;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
            .chatbox-header img {
                border-radius: 50%;
                margin-right: 10px;
            }
            .chatbox-body {
                padding: 15px;
                height: 300px;
                overflow-y: auto;
                background: #ece5dd;
            }
            .chatbox-message {
                display: flex;
                margin-bottom: 10px;
            }
            .message-sent {
                justify-content: flex-end;
            }
            .message-text {
                padding: 10px;
                border-radius: 20px;
                font-size: 14px;
                margin: 5px;
                position: relative;
            }
            .message-received {
                align-items: flex-start;
            }
            .message-received .message-text {
                background-color: #ffffff;
                margin-left: 10px;
            }
            .message-sent .message-text {
                background-color: #dcf8c6;
            }
            .chatbox-message img.avatar {
                width: 30px;
                height: 30px;
                border-radius: 50%;
            }
            .message-text img.message-image {
                max-width: 200px;
                border-radius: 10px;
                display: block;
                margin-top: 5px;
            }
            .chatbox-footer {
                background-color: #f0f0f0;
                padding: 10px 15px;
                display: flex;
                gap: 3px;
                align-items: center;
            }
            .chatbox-footer input {
                flex-grow: 1;
                padding: 10px;
                border-radius: 20px;
                border: 1px solid #ccc;
                margin-right: 10px;
            }
            .chatbox-footer button {
                padding: 10px 15px;
                border: none;
                border-radius: 20px;
                background-color: #5b36ac;
                color: white;
                cursor: pointer;
            }
            .chatbox-footer img.file-img {
                width: 30px;
                border-radius: 50%;
            }
        </style>
    </head>
    <body data-current-userid="<c:out value='${receive.accountid}'/>">


        <div class="chatbox-container">


            <div class="chatbox-header">
                <div>
                    <c:set var="u" value="${receive}">
                    </c:set>
                    <img id="receiveAvatar" src="${u.avatar}" alt="Avatar" width="40">
                    <span>${u.userName} -- ${boxChat}</span>


                </div>

            </div>

            <div id="loadData" class="chatbox-body">
                <c:if test="${list!=null}">

                    <c:forEach var="m" items="${list}">

                        <!-- Received message withddddddddddddddddddddddddddddddddddddddddddddd text and image -->
                        <c:if test="${m.senderId.accountid == u.accountid}">
                            <div class="chatbox-message message-received">
                                <img class="avatar" src="${m.senderId.avatar}" alt="Other's Avatar">
                                <div class="message-text">
                                    <c:if test="${m.message!=null}">
                                        ${m.message}
                                    </c:if>

                                    <c:if test="${m.image!=null}">
                                        <img src="${m.image}" alt="Other's Image" class="message-image">
                                    </c:if>
                                </div>
                            </div>
                        </c:if>


                        <c:if test="${m.senderId.accountid != u.accountid}">

                            <div class="chatbox-message  message-sent">

                                <div class="message-text">
                                    <c:if test="${m.message!=null}">
                                        ${m.message}
                                    </c:if>

                                    <c:if test="${m.image!=null}">
                                        <img src="${m.image}" alt="Other's Image" class="message-image">
                                    </c:if>
                                </div>
                            </div>

                        </c:if>


                    </c:forEach>
                </c:if>

            </div>



            <!--<img src="image/accept.png">-->

            <div class="chatbox-footer">
                <!--<form id="chatForm" style="display: flex" method="post" enctype="multipart/form-data" data-boxchat="${boxChat}" data-receive="${receive.accountid}">-->
                <form  id="chatForm" style="display: flex" method="post" enctype="multipart/form-data" action="sentMessage?boxChat=${boxChat}&receive=${receive.accountid}">
                    <span style="width: 34px" class="attach_btn2k3">  <img class="file-img" src="image/camera.jpg">
                    </span>
                    <div style="position: relative;">

                        <input hidden id="updateImage" name="imageLast" value="${image.get(0).image}">
                        <div style="position: absolute;bottom:45px ;" class="upload-img-preview">

                        </div>
                        <input hidden type="file" name="messageImage" class="visually-hidden" id="upload-input1" >

                        <input style="width: 500px" type="text" name="messageText" class="type_msg" placeholder="Type your message...">
                    </div>

                    <span class=" send_btn_mes"><img style="width: 30px" src="icons/mic.svg"></span>
                    <input id="sent_btn_mes_id" type="submit" hidden>

                </form>

            </div>





            <script>





                document.addEventListener('DOMContentLoaded', function () {
                    // Khởi tạo WebSocket
                    var ws = new WebSocket('ws://localhost:9999/swp391/chat1');

                    ws.onmessage = function (event) {
                        // Cập nhật chatbox khi có tin nhắn mới
                        var message = JSON.parse(event.data);
                        addMessageToChatbox(message);
                    };

                    var form = document.getElementById('chatForm');

                    form.addEventListener('submit', function (e) {
                        e.preventDefault(); // Ngăn chặn form submit thông thường

                        var formData = new FormData(this);
                        var imageFile = document.getElementById('upload-input1').files[0]; // Lấy file ảnh từ input

                        // Kiểm tra nếu có ảnh và tạo URL
                        var imageURL = imageFile ? URL.createObjectURL(imageFile) : '';
//console.log("anh cua toi :" +imageURL);
                        var message = {
                            senderId: document.body.getAttribute('data-current-userid'),
                            message: formData.get('messageText'),
                            image: imageURL // Sử dụng URL ảnh
                        };
                        ws.send(JSON.stringify(message)); // Chuyển tin nhắn thành chuỗi JSON và gửi

                        // Gửi dữ liệu form qua AJAX (bao gồm cả ảnh)
                        formData.append('messageImage', imageFile); // Thêm file ảnh vào formData nếu có


                        fetch(this.action, {
                            method: 'POST',
                            body: formData
                        }).then(response => response.text())
                                .then(() => {

                                    document.querySelectorAll('.upload-img-preview').forEach(preview => {
                                        preview.innerHTML = '';
                                    });
                                    scrollToBottom();
                                })
                                .catch(error => console.error('Error:', error));

                        // Reset các trường của form
                        this.reset();
                    });

                    function addMessageToChatbox(message) {
                        var chatbox = document.getElementById('loadData');
                        var avatarSrc = document.getElementById('receiveAvatar').getAttribute('src');
                        var messageDiv = document.createElement('div');
                        messageDiv.className = 'chatbox-message ' + (message.senderId === document.body.getAttribute('data-current-userid') ? 'message-sent' : 'message-received');

                        // Tạo nội dung tin nhắn và kiểm tra để thêm avatar cho người nhận
                        var contentHtml = (message.senderId === document.body.getAttribute('data-current-userid') ? '' : '<img class="avatar" src="' + avatarSrc + '" alt="Other\'s Avatar">');

                        contentHtml += '<div class="message-text">' + (message.message ? message.message : "<span class=\"like_btn\" \"><i class=\"fas fa-thumbs-up\" style=\"font-size:24px;\"></i></span>\n" + "");

                        // Kiểm tra nếu có ảnh trong tin nhắn và thêm vào nội dung
                        if (message.image) {
                            contentHtml += '<img src="' + message.image + '" alt="Message Image" class="message-image">';
                        }
                        contentHtml += '</div>';

                        messageDiv.innerHTML = contentHtml;
                        chatbox.appendChild(messageDiv);
                        scrollToBottom();
                    }


                    function scrollToBottom() {
                        var chatbox = document.getElementById('loadData');
                        chatbox.scrollTop = chatbox.scrollHeight;
                    }
                });












                function scrollToBottom() {
                    var chatboxBody = document.querySelector('.chatbox-body');
                    chatboxBody.scrollTop = chatboxBody.scrollHeight;
                }


                window.onload = scrollToBottom;



                document.querySelector(".attach_btn2k3").addEventListener("click", function () {
                    document.getElementById("upload-input1").click();
                });


                document.querySelector(".send_btn_mes").addEventListener("click", function () {
                    document.getElementById("sent_btn_mes_id").click();
                });



                document.getElementById("upload-input1").addEventListener("change", function (e) {
                    const file = e.target.files[0]; // Lấy file đầu tiên được chọn

                    if (file) {
                        if (file.size > 10485760) { // 10MB = 10 * 1024 * 1024 = 10485760 bytes
                            alert("File quá lớn, vui lòng chọn file dưới 10MB.");
                            return; // Ngừng xử lý nếu file quá lớn
                        }

                        const reader = new FileReader();
                        reader.onload = function (e) {
                            const imgContainer = document.createElement("div");
                            imgContainer.style.position = "relative";

                            const img = document.createElement("img");
                            img.src = e.target.result;
                            img.style.maxWidth = "80px";
                            img.style.maxHeight = "80px";

                            const removeBtn = document.createElement("span");
                            removeBtn.innerHTML = "&times;";
                            removeBtn.style.position = "absolute";
                            removeBtn.style.top = "0";
                            removeBtn.style.right = "0";
                            removeBtn.style.cursor = "pointer";
                            removeBtn.style.color = "yellow";
                            removeBtn.style.fontSize = "20px";
                            removeBtn.style.fontWeight = "bold";

                            removeBtn.onclick = function () {
                                imgContainer.remove();
                                document.getElementById("upload-input1").value = ""; // Reset input file
                            };

                            imgContainer.appendChild(img);
                            imgContainer.appendChild(removeBtn);
                            document.querySelector(".upload-img-preview").appendChild(imgContainer);
                        };
                        reader.readAsDataURL(file);
                    }
                });



            </script>
    </body>
</html>
