**MyOnlineTransaction**__


This project can help users make intermediary transactions through cyberspace securely and conveniently without having to meet face to face.
Installation
Software requirement: 

****Apache Tomcat 10.0.27

 JDK17.0


 MySQL


 Apache Netbeans 13 or later**

Link to download database: **https://drive.google.com/file/d/1H5F_oSA40Mnn7WVGrRHkkvFAyIqIRUhb/view?usp=sharing**
 


Libraries: You need to add some libraries, I have them available in the Lib section, please add them to run the project.

Database: You will download script here and execute it to create a database with previously added values for the operation of the web application.

You can clone this project to run localhost from your machine via:

HTTPS

**https://gitlab.com/hoangphihe17/myonlinetransaction.git**
or

Gitlab CLI

**git@gitlab.com:hoangphihe17/myonlinetransaction.git**

Usage

Step 1: Change your SQL account to make a connection to the database at **\src\java\dal\DBContext.java**

Step 2: Execute the **database_swp.txt** you have downloaded before.

Step 3: Add project to one of IDEs in** Software requirement** which I've listed above.

Step 4: Run the project, you can register a new account or use an existing account in the database to log in (All login information is saved in the Users table).

Note: Due to security reason, I have to hide client id and client secret key, so** "login/register with Google" **feature will not available, if you want to test this feature, please contact me to get these keys: email: nguyenhoangphi192@gmail.comcom or phinhhe172792@fpt.edu.vn

