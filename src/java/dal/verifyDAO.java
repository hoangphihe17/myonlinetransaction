/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Truong cun
 */
public class verifyDAO extends DBContext{
    
    public boolean checkOTP(String otp , int accountid){
        String sql ="select * from verify where otp=? and accountid =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, otp);
            st.setInt(2, accountid);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return false;
    }
    
    public void updateOTP(String otp , int accountid){
        String sql ="update verify set otp=? where accountId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, otp);
            st.setInt(2, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    public void insertOTP(String otp , int accountid){
        String sql ="INSERT INTO [dbo].[verify]\n" +
"           ([otp]\n" +
"           ,[accountId])\n" +
"     VALUES\n" +
"           (?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, otp);
            st.setInt(2, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void main(String[] args) {
        verifyDAO d = new verifyDAO();
        System.out.println(d.checkOTP("12345644", 1));
    }
}
