/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.hearted;

/**
 *
 * @author Truong cun
 */
public class heartDAO extends DBContext {

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void insertHeart(String userName, String productid) {
        String sql = "INSERT INTO [dbo].[hearted]\n"
                + "           ([userName]\n"
                + "          \n"
                + "           ,[createAt]\n"
                + "           ,[productId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, userName);
            st.setString(2, formattedDateTime);
            st.setString(3, productid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteHeart(String userName, String productid) {
        String sql = "delete from [hearted] where username = ? and productid =?";

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, userName);

            st.setString(2, productid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<hearted> getListHearted(String productid) {
        List<hearted> list = new ArrayList<>();
        accountDAO a = new accountDAO();
        String sql = "select * from hearted where productid =?  order by id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, productid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account b = a.acc(rs.getString("userName"));
                hearted d = new hearted(rs.getInt("id"), b, rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6));
                list.add(d);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        heartDAO d = new heartDAO();
//        System.out.println(d.getListHearted("BjgnyMAKgb6dPod").get(0).getUserName().getAccountid());
//d.deleteHeart("user2", "BjgnyMAKgb6dPod");
//d.insertHeart("user2", "BjgnyMAKgb6dPod");
    }
}
