/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.chatCommunity;

/**
 *
 * @author Truong cun
 */
public class chatCommunityDAO extends DBContext {

    public List<chatCommunity> getAllList() {
        List<chatCommunity> list = new ArrayList<>();
        String sql = "select c.id as cid , c.message , c.createAt as cCreateAt , c.updateAt as cUpdateAt , a.*\n"
                + "from chatCommunity c join account a on c.accountId = a.id order by c.id asc\n"
                + "\n"
                + "";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
                chatCommunity c = new chatCommunity(rs.getInt("cid"), rs.getString("message"), rs.getString("cCreateAt"),
                        rs.getString("cUpdateAt"), a);
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void addChatCommunity(int accountid, String mes) {
        String sql = "INSERT INTO [dbo].[chatCommunity]\n"
                + "           ([message]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt]\n"
                + "           ,[accountId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, mes);
            st.setString(2, formattedDateTime);

            st.setString(3, formattedDateTime);

            st.setInt(4, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public static void main(String[] args) {
        chatCommunityDAO c = new chatCommunityDAO();
//        System.out.println(c.getAllList().get(0).getMessage());
//                System.out.println(c.addChatCommunity("4","mua nic");
//c.addChatCommunity("4", "bán nic giá avatar 48 ô đất , 1tr vnd , ib zalo 05646464646");

    }

}
