/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import model.account;

/**
 *
 * @author Truong cun
 */
public class accountDAO extends dal.DBContext {

    public void register(account a) {
        String sql = "insert into account (userName, email , password,role,code,status) "
                + "values(?,?,?,?,?,?)";

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getUserName());
            st.setString(2, a.getEmail());
            st.setString(3, a.getPassword());
            st.setString(4, "user");
            st.setString(5, a.getCode());
            st.setString(6, a.getStatus());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public void updatePass(account a) {
        String sql = "UPDATE [dbo].[account]\n"
                + "   SET password = ?"
                + " WHERE username=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getPassword());
            st.setString(2, a.getPassword());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        //insert data
    }

    public void updateCode(String code, String user) {
        String sql = "UPDATE [dbo].[account]\n"
                + "   SET code = ?"
                + " WHERE username=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, code);
            st.setString(2, user);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<account> getAllList() {
        List<account> list = new ArrayList<>();
        String sql = "select * from account ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<account> findTransfer(String userName , int id) {
        List<account> list = new ArrayList<>();
        String sql = "select * from account where 1=1 and id != ?";

        if (userName != null && !userName.isEmpty()) {
            sql += " and userName like '%" + userName + "%'";

        } else {
            sql += " and userName like '%@#$%^%'";

        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public void updatePass1(String user, String pass) {
        String sql = "UPDATE [dbo].[account]\n"
                + "   SET password = ?"
                + " WHERE username=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, user);

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public account check(String u, String p) {
        String sql = "select * from account where userName =? and password =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);
            st.setString(2, p);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public boolean checkexits(String u) {
        String sql = "select * from account where userName =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {

                return true;
            }
        } catch (SQLException e) {
        }

        return false;
    }

    public account getAccount(int u) {
        String sql = "select * from account where id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, u);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {

                return new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }
    public account getAccount1(String id) {
        String sql = "select * from account where id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {

                return new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public boolean checkexitsEmail(String u) {
        String sql = "select * from account where email =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {

                return true;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    
    public void changeAvt(String img,int accountid){
        String sql ="update account set avatar = ?  where id = ?";
        try {
            PreparedStatement st =connection.prepareStatement(sql);
            st.setString(1, img);
            st.setInt(2, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public void updatePro5(account a) {
        String sql = "UPDATE [dbo].[account]\n"
                + "   SET"
                + " code = ?"
                + "      ,status = ?"
                + " WHERE userName=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, a.getCode());
            st.setString(2, a.getStatus());
            st.setString(3, a.getUserName());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public account acc(String user) {
        String sql = "select * from account where userName =?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new account(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void updateMoney(double money, int accountid) {
        String sql = "UPDATE [dbo].[account]\n"
                + "   SET \n"
                + "      [address] = ?\n"
                + "      \n"
                + "      ,[updateAt] =?\n"
                + " WHERE id = ?";

        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, money);
            st.setString(2, formattedDateTime);
            st.setInt(3, accountid);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

 

    public static void main(String[] args) {
        accountDAO a = new accountDAO();
        System.out.println(a.findTransfer("u", 1).size());
//        System.out.println(a.findTransfer("").size());
//a.changeAvt("image/akaza1.jpg", "1");
//        a.updateMoney("700000", 1);
//        System.out.println(a.getAllList().get(0).getId());
//        UUID uuid = UUID.randomUUID();
//        System.out.println(uuid.toString());
    }
}
