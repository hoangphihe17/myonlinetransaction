/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.weekDay;

/**
 *
 * @author Truong cun
 */
public class ratingDAO extends DBContext {

    public List<weekDay> listStar(int accountid) {
        List<weekDay> list = new ArrayList<>();
        String sql = "SELECT \n"
                + "    numbers.starNo, \n"
                + "    COALESCE(rating_count.number, 0) as number \n"
                + "FROM \n"
                + "    (\n"
                + "        SELECT 1 AS starNo UNION ALL\n"
                + "        SELECT 2 AS starNo UNION ALL\n"
                + "        SELECT 3 AS starNo UNION ALL\n"
                + "        SELECT 4 AS starNo UNION ALL\n"
                + "        SELECT 5 AS starNo\n"
                + "    ) AS numbers\n"
                + "LEFT JOIN \n"
                + "    (\n"
                + "        SELECT \n"
                + "            r.starNo, \n"
                + "            COUNT(r.id) AS number \n"
                + "        FROM \n"
                + "            rating r \n"
                + "        JOIN \n"
                + "            account a ON a.id = r.accountId \n"
                + "        JOIN \n"
                + "            product p ON p.id = r.productId \n"
                + "        WHERE \n"
                + "            p.accountId = ?\n"
                + "        GROUP BY \n"
                + "            r.starNo\n"
                + "    ) AS rating_count ON numbers.starNo = rating_count.starNo;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountid);
            ResultSet rs= st.executeQuery();
            while(rs.next()){
                weekDay a = new weekDay(rs.getString("starNo"), rs.getString("number"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public static void main(String[] args) {
        ratingDAO r = new ratingDAO();
        System.out.println(r.listStar(1).get(0).getRevenue());
    }
}
