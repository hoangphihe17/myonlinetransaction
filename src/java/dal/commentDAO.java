/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.comment;

public class commentDAO extends DBContext {

    public List<comment> getCommentByProductID(String id) {
        List<comment> list = new ArrayList<>();
        String sql = "SELECT \n"
                + "  c.id AS comment_id, \n"
                + "  c.content, \n"
                + "  c.createAt AS comment_createAt, \n"
                + "  c.updateAt AS comment_updateAt, \n"
                + "  c.sortDelete AS comment_sortDelete, \n"
                + "  c.productId, \n"
                + "  c.deleteBy as comment_deleteBy , \n"
                + "  c.accountId,\n"
                + "  a.id AS account_id, \n"
                + "  a.userName, \n"
                + "  a.password, \n"
                + "  a.email, \n"
                + "  a.address, \n"
                + "  a.phone, \n"
                + "  a.avatar, \n"
                + "  a.role, \n"
                + "  a.band, \n"
                + "  a.code, \n"
                + "  a.status, \n"
                + "  a.sortDelete AS account_sortDelete, \n"
                + "  a.createAt AS account_createAt, \n"
                + "  a.updateAt AS account_updateAt\n"
                + "FROM \n"
                + "  comment c\n"
                + "JOIN \n"
                + "  account a\n"
                + "ON \n"
                + "  c.accountId = a.id\n"
                + "where c.sortDelete ='no' and c.productId =? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                // Khởi tạo một đối tượng Account từ dữ liệu cột liên quan
                account account = new account();
                account.setAccountid(rs.getInt("account_id"));
                account.setUserName(rs.getString("userName"));
                account.setPassword(rs.getString("password"));
                account.setEmail(rs.getString("email"));
                account.setAddress(rs.getString("address"));
                account.setPhone(rs.getString("phone"));
                account.setAvatar(rs.getString("avatar"));
                account.setRole(rs.getString("role"));
                account.setBand(rs.getString("band"));
                account.setCode(rs.getString("code"));
                account.setStatus(rs.getString("status"));
                account.setSortDelete(rs.getString("account_sortDelete"));
                account.setCreateAt(rs.getString("account_createAt"));
                account.setUpdateAt(rs.getString("account_updateAt"));
// Khởi tạo một đối tượng Comment từ dữ liệu cột liên quan
                comment comment = new comment();
                comment.setId(rs.getInt("comment_id"));
                comment.setContent(rs.getString("content"));
                comment.setCreateAt(rs.getString("comment_createAt"));
                comment.setUpdateAt(rs.getString("comment_updateAt"));
                comment.setSortDelete(rs.getString("comment_sortDelete"));
                comment.setSortDelete(rs.getString("comment_deleteBy"));

                comment.setProductid(rs.getString("productId"));
                comment.setAccountid(account);

// Thêm các đối tượng đã khởi tạo vào danh sách
                list.add(comment);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;

    }
    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void addComment(String content, String productId, int accountid) {
        String sql = "INSERT INTO [dbo].[comment]\n"
                + "           ([content]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt]\n"
                + "           ,[sortDelete]\n"
                + "           ,[productId]\n"
                + "           ,[accountId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, formattedDateTime);
            st.setString(3, formattedDateTime);
            st.setString(4, "no");

            st.setString(5, productId);
            st.setInt(6, accountid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateComment(String id, String content) {
        String sql = "UPDATE [dbo].[comment]\n"
                + "   SET [content] =?\n"
                + "      ,[updateAt] =?\n"
                + " WHERE id = ?";
        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, formattedDateTime);
            st.setString(3, id);
            st.executeUpdate();
            //catch ra exception nếu sai
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteComment(String id, String deleteBy) {
        String sql = "UPDATE [dbo].[comment]\n"
                + "   SET [updateAt] = ?\n"
                + "      ,[sortDelete] = ?\n"
                + "    \n"
                + "      ,[deleteBy] = ?\n"
                + " WHERE id =?";
        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, formattedDateTime);
            st.setString(2, "yes");
            st.setString(3, deleteBy);
            st.setString(4, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        commentDAO c = new commentDAO();
        System.out.println(c.getCommentByProductID("rKTg1xxf8WsvBdr").get(0).getAccountid().getAccountid());
//        c.addComment("truong cun", "hjEVpF1crli9MRT", 2);
//        c.updateComment("15", "truong cun 10d");
//        c.deleteComment("15", "admin");
    }
}
