/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.product;
import model.productImage;

/**
 *
 * @author Truong cun
 */
public class productImageDAO extends DBContext {

    public List<productImage> getAllList() {
        List<productImage> list = new ArrayList<>();
        String sql = "select i.id as iID, name , path , i.createdAt as icreate , i.updatedAt"
                + " as iupdate , p.* from"
                + " productImages i join product p on i.productId =p.id";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                account a = new account();
                product p = new product(rs.getString("id"), rs.getString("productName"), rs.getString("fee"), rs.getString("describe"),
                        rs.getString("infomation"), rs.getInt("like"), rs.getInt("view"), rs.getInt("quantity"),
                        rs.getString("price"), rs.getString("privateInfo"), rs.getString("publicPrivate"), rs.getString("status"),
                        rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"), rs.getInt("categoryId"), a);

                productImage i = new productImage(rs.getInt("iID"), rs.getString("name"),
                        rs.getString("path"), rs.getString("icreate"), rs.getString("iupdate"),
                        p);

                list.add(i);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<productImage> listImageByProductId(String id) {
        List<productImage> list = new ArrayList<>();
        String sql = "select * from productImages where productId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                product p = new product();

                productImage i = new productImage(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5),
                        p);

                list.add(i);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void deleteImage(String productid) {
        String sql = "delete from productImages where productId =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, productid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void addImage(String productId, String path) {
        String sql = "INSERT INTO [dbo].[productImages]\n"
                + "           ([name]\n"
                + "           ,[path]\n"
                + "           ,[createdAt]\n"
                + "           ,[updatedAt]\n"
                + "           ,[productId])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "o");
            st.setString(2,  path);
            st.setString(3, formattedDateTime);
            st.setString(4, formattedDateTime);
            st.setString(5, productId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        productImageDAO d = new productImageDAO();
        d.deleteImage("13yIuFKaLMhs1vw");
//        System.out.println(d.listImageByProductId("Xqc70xhUd3YS0NG").get(0).getPath());
//        d.addImage("13yIuFKaLMhs1vw", "image/akaza1.jpg");
    }
}
