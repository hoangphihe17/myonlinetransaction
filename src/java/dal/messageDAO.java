/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.boxChat;
import model.message;

public class messageDAO extends DBContext {

    LocalDateTime currentDateTime = LocalDateTime.now();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public List<message> getMessageByBoxChat(int id) {
        List<message> list = new ArrayList<>();
        messageDAO m1 = new messageDAO();
        accountDAO ac = new accountDAO();

        String sql = "select * from message where boxChatId = ? order by id asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                boxChat b = m1.getBoxChat(rs.getInt("boxChatid"));
                int u1 = rs.getInt("Senderid");

                account user1 = ac.getAccount(u1);

                message m = new message(rs.getInt("id"), b, user1, rs.getString("image"),
                        rs.getString("message"), rs.getString("createAt"), rs.getString("updateAt"));
                list.add(m);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }
    public List<message> getMessageByBoxChat1(int id) {
        List<message> list = new ArrayList<>();
        messageDAO m1 = new messageDAO();
        accountDAO ac = new accountDAO();

        String sql = "select  top 1 * from message where boxChatId = ? order by id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                boxChat b = m1.getBoxChat(rs.getInt("boxChatid"));
                int u1 = rs.getInt("Senderid");

                account user1 = ac.getAccount(u1);

                message m = new message(rs.getInt("id"), b, user1, rs.getString("image"),
                        rs.getString("message"), rs.getString("createAt"), rs.getString("updateAt"));
                list.add(m);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    public boxChat getBoxChat(int id) {

        String sql = "select * from boxChat where id = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                accountDAO ac = new accountDAO();

                int u1 = rs.getInt("user1");
                int u2 = rs.getInt("user2");
                account user1 = ac.getAccount(u1);
                account user2 = ac.getAccount(u2);
                return new boxChat(rs.getInt(1), user1, user2, rs.getString(4), rs.getString(5));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public List<message> getMessage() {
        List<message> list = new ArrayList<>();
        String sql = "select m.id as mid , m.boxChatid , m.Senderid , m.image , m.message , m.createAt as mCreate , m.updateAt as mUpdate , b.id as  bid , user1 , user2 ,b.createAt as bCreate ,\n"
                + "b.updateAt as bUpdate , a.*\n"
                + "from message m join boxChat b on b.id = m.boxChatid join account a on m.Senderid = a.id";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));

                accountDAO ac = new accountDAO();
                int u1 = rs.getInt("user1");
                int u2 = rs.getInt("user2");
//                System.out.println(u1 +"-"+ u2);
                account user1 = ac.getAccount(u1);
                account user2 = ac.getAccount(u2);

                boxChat b = new boxChat(rs.getInt("bid"), user1, user2, rs.getString("bCreate"), rs.getString("bUpdate"));

                message m = new message(rs.getInt("mid"), b, a, rs.getString("image"),
                        rs.getString("message"), rs.getString("mCreate"), rs.getString("mUpdate"));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<message> listAccountMess(int id) {
        List<message> list = new ArrayList<>();
        String sql = "SELECT m.id as mid, m.boxChatid, m.Senderid, m.image, m.message, m.createAt as mCreate, m.updateAt as mUpdate, \n"
                + "       b.id as bid, user1, user2, b.createAt as bCreate, b.updateAt as bUpdate, a.*\n"
                + "FROM message m\n"
                + "JOIN (\n"
                + "    SELECT boxChatid, MAX(createAt) as LastMessageTime\n"
                + "    FROM message\n"
                + "    GROUP BY boxChatid\n"
                + ") lm ON m.boxChatid = lm.boxChatid AND m.createAt = lm.LastMessageTime\n"
                + "JOIN boxChat b ON m.boxChatid = b.id\n"
                + "JOIN account a ON m.Senderid = a.id\n"
                + "WHERE (b.user1 =? OR b.user2 = ?) "
                + " ORDER BY m.createAt DESC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setInt(2, id);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                account a = new account(rs.getInt("id"), rs.getString("userName"), rs.getString("password"), rs.getString("email"), rs.getString("address"),
                        rs.getString("phone"), rs.getString("avatar"), rs.getString("role"), rs.getString("band"), rs.getString("code"),
                        rs.getString("status"), rs.getString("sortDelete"), rs.getString("createAt"), rs.getString("updateAt"));

                accountDAO ac = new accountDAO();
                int u1 = rs.getInt("user1");
                int u2 = rs.getInt("user2");
//                System.out.println(u1 +"-"+ u2);
                account user1 = ac.getAccount(u1);
                account user2 = ac.getAccount(u2);

                boxChat b = new boxChat(rs.getInt("bid"), user1, user2, rs.getString("bCreate"), rs.getString("bUpdate"));

                message m = new message(rs.getInt("mid"), b, a, rs.getString("image"),
                        rs.getString("message"), rs.getString("mCreate"), rs.getString("mUpdate"));
                list.add(m);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }
    
    
    

    public void addBoxChat(int sender, int receive) {
        String sql = "INSERT INTO [dbo].[boxChat]\n"
                + "           ([user1]\n"
                + "           ,[user2]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sender);
            st.setInt(2, receive);
            st.setString(3, formattedDateTime);
            st.setString(4, formattedDateTime);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public boxChat checkBoxChat(int sender, int receive) {
        String sql = "select * from boxChat where user1 =? and user2 =?";

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sender);
            st.setInt(2, receive);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                  accountDAO ac = new accountDAO();

                int u1 = rs.getInt("user1");
                int u2 = rs.getInt("user2");
                account user1 = ac.getAccount(u1);
                account user2 = ac.getAccount(u2);
                return new boxChat(rs.getInt(1), user1, user2, rs.getString(4), rs.getString(5));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public void addMess(int boxchat, int senderid, String image, String message) {
        String sql = "INSERT INTO [dbo].[message]\n"
                + "           ([boxChatid]\n"
                + "           ,[Senderid]\n"
                + "           ,[image]\n"
                + "           ,[message]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, boxchat);
            st.setInt(2, senderid);
            st.setString(3, "image/" + image);
            st.setString(4, message);
            st.setString(5, formattedDateTime);
            st.setString(6, formattedDateTime);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void addMess2(int boxchat, int senderid, String image) {
        String sql = "INSERT INTO [dbo].[message]\n"
                + "           ([boxChatid]\n"
                + "           ,[Senderid]\n"
                + "           ,[image]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, boxchat);
            st.setInt(2, senderid);
            st.setString(3, "image/" + image);

            st.setString(4, formattedDateTime);
            st.setString(5, formattedDateTime);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void addMess1(int boxchat, int senderid, String message) {
        String sql = "INSERT INTO [dbo].[message]\n"
                + "           ([boxChatid]\n"
                + "           ,[Senderid]\n"
                + "           ,[message]\n"
                + "           ,[createAt]\n"
                + "           ,[updateAt])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            String formattedDateTime = currentDateTime.format(formatter);

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, boxchat);
            st.setInt(2, senderid);

            st.setString(3, message);
            st.setString(4, formattedDateTime);
            st.setString(5, formattedDateTime);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        messageDAO m = new messageDAO();
        System.out.println(m.getMessageByBoxChat1(1).get(0).getImage());
    }

}
