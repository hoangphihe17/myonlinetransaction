/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class hearted {
    int id ; 
    account userName;
    String softDelete , deleteBy, createAt , productid;

    public hearted() {
    }

    public hearted(int id, account userName, String softDelete, String deleteBy, String createAt, String productid) {
        this.id = id;
        this.userName = userName;
        this.softDelete = softDelete;
        this.deleteBy = deleteBy;
        this.createAt = createAt;
        this.productid = productid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public account getUserName() {
        return userName;
    }

    public void setUserName(account userName) {
        this.userName = userName;
    }

    public String getSoftDelete() {
        return softDelete;
    }

    public void setSoftDelete(String softDelete) {
        this.softDelete = softDelete;
    }

    public String getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(String deleteBy) {
        this.deleteBy = deleteBy;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
    
}
