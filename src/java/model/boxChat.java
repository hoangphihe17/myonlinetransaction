/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class boxChat {
    int id ;
    account user1 , user2;
    String createAt , updateAt ;

    public boxChat() {
    }

    public boxChat(int id, account user1, account user2, String createAt, String updateAt) {
        this.id = id;
        this.user1 = user1;
        this.user2 = user2;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public account getUser1() {
        return user1;
    }

    public void setUser1(account user1) {
        this.user1 = user1;
    }

    public account getUser2() {
        return user2;
    }

    public void setUser2(account user2) {
        this.user2 = user2;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }
    
    
}
