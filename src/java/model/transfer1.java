/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class transfer1 {
    int id ;
String money ,content, createAt ,status  ;
account senderId, receiveId;

    public transfer1() {
    }

    public transfer1(int id, String money, String content, String createAt, String status, account senderId, account receiveId) {
        this.id = id;
        this.money = money;
        this.content = content;
        this.createAt = createAt;
        this.status = status;
        this.senderId = senderId;
        this.receiveId = receiveId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public account getSenderId() {
        return senderId;
    }

    public void setSenderId(account senderId) {
        this.senderId = senderId;
    }

    public account getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(account receiveId) {
        this.receiveId = receiveId;
    }



}
