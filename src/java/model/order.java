/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


public class order {
    int id;
    String price ;
    int quantity;
    String note  , orderdate, success, sortDelete;
     product productid;
    account accountid;

    public order() {
    }

    public order(int id, String price, int quantity, String note, String orderdate, String success, String sortDelete, product productid, account accountid) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.note = note;
        this.orderdate = orderdate;
        this.success = success;
        this.sortDelete = sortDelete;
        this.productid = productid;
        this.accountid = accountid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSortDelete() {
        return sortDelete;
    }

    public void setSortDelete(String sortDelete) {
        this.sortDelete = sortDelete;
    }

    public product getProductid() {
        return productid;
    }

    public void setProductid(product productid) {
        this.productid = productid;
    }

    public account getAccountid() {
        return accountid;
    }

    public void setAccountid(account accountid) {
        this.accountid = accountid;
    }

    
}
