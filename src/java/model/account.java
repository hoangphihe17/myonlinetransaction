/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Truong cun
 */
public class account {

    int accountid;
    String userName, password, email, address, phone, avatar,
            role, band, code, status, sortDelete, createAt, updateAt;

    public account() {
    }

    public account(int id, String userName, String password, String email,
            String address, String phone, String avatar, String role, String band, String code, String status,
            String sortDelete, String createAt, String updateAt) {
        this.accountid = id;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.avatar = avatar;
        this.role = role;
        this.band = band;
        this.code = code;
        this.status = status;
        this.sortDelete = sortDelete;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public account(String userName, String code, String status, String updateAt) {
        this.userName = userName;

        this.code = code;
        this.status = status;
        this.updateAt = updateAt;

    }

    public account(String userName, String email, String password, String role, String code,
            String status) {
        this.userName = userName;

        this.email = email;
        this.password = password;
        this.role = role;

        this.code = code;
        this.status = status;
        
    }

    public account(String userName, String password, String role) {

        this.userName = userName;
        this.password = password;

        this.role = role;

    }

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSortDelete() {
        return sortDelete;
    }

    public void setSortDelete(String sortDelete) {
        this.sortDelete = sortDelete;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

}
