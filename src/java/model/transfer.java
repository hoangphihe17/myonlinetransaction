
package model;


public class transfer {
  int id ;
String money ,content, createAt ,status , senderId ;
account receiveId;

    public transfer() {
    }

    public transfer(int id, String money, String content, String createAt, String status, String senderId, account receiveId) {
        this.id = id;
        this.money = money;
        this.content = content;
        this.createAt = createAt;
        this.status = status;
        this.senderId = senderId;
        this.receiveId = receiveId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public account getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(account receiveId) {
        this.receiveId = receiveId;
    }



}
