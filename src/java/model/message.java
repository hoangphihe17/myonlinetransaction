package model;

/**
 *
 * @author Truong cun
 */
public class message {

    int id;
    boxChat boxChatId;
    account senderId;
    String image, message;
    String createAt, updateAt;

    public message() {
    }

    public message(int id, boxChat boxChatId, account senderId, String image, String message, String createAt, String updateAt) {
        this.id = id;
        this.boxChatId = boxChatId;
        this.senderId = senderId;
        this.image = image;
        this.message = message;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boxChat getBoxChatId() {
        return boxChatId;
    }

    public void setBoxChatId(boxChat boxChatId) {
        this.boxChatId = boxChatId;
    }

    public account getSenderId() {
        return senderId;
    }

    public void setSenderId(account senderId) {
        this.senderId = senderId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    
    
    
    
}
