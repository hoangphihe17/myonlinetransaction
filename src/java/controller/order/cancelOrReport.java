/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import controller.home.NotificationEndpoint;
import dal.accountDAO;
import dal.moneyDAO;
import dal.notificationDAO;
import dal.orderDAO;
import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import model.account;
import model.order;
import model.product;
import model.queue;
import model.trading;

/**
 *
 * @author Truong sssssssssssssssssssssssscun
 */
@WebServlet(name = "cancelOrReport", urlPatterns = {"/cancelOrReport"})
public class cancelOrReport extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet cancelOrReport</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet cancelOrReport at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String id = request.getParameter("id");
        productDAO d = new productDAO();
        orderDAO o = new orderDAO();
        List<order> o1 = o.getOrderByProductid(id);
        product p = d.getProductbyID(id);
        moneyDAO m = new moneyDAO();
        accountDAO dd = new accountDAO();
        account poster = dd.getAccount(p.getAccountId().getAccountid());
        account cancel = dd.getAccount(o1.get(0).getAccountid().getAccountid());
        notificationDAO no = new notificationDAO();
        String result = p.getPrice().split("\\.")[0];
        long price1 = Long.parseLong(result);

        HttpSession s = request.getSession();
        account checkac = (account) s.getAttribute("account");

        if (checkac == null) {
            response.sendRedirect("login");
        } else if (o1.get(0).getAccountid().getAccountid() != checkac.getAccountid()
                && p.getAccountId().getAccountid() != checkac.getAccountid()) {
            response.sendRedirect("home");
            ///TH khác người mua và người bán;
        } else {
            if ("Wait".equalsIgnoreCase(p.getStatus())) {
                response.sendRedirect("home");
            } else {
                NumberFormat formatter = NumberFormat.getInstance(new Locale("vi", "VN"));

                queue.getMyrequest().add(new trading(o1.get(0).getAccountid().getAccountid(), price1));

                while (!queue.getMyrequest().isEmpty()) {
                    trading a = queue.getMyrequest().getFirst();
                    account c = dd.getAccount(a.getAccountId());

                    //  ============nguoi mua huy============
                    long amount = Long.parseLong(c.getAddress());
                    double aaa = a.getAmount() * 0.90;
                    String result1 = String.valueOf(aaa).split("\\.")[0];
                    long price = Long.parseLong(result1);
                    m.insertTransfer1(price, "90% refund code orders " + p.getId(), 3, c.getAccountid());
                    dd.updateMoney(amount + price, c.getAccountid());
                    String formattedNumber = formatter.format(price);
                    String formattedNumber1 = formatter.format(amount + price);

                    //  ============nguoi ban============
                    account c1 = dd.getAccount(p.getAccountId().getAccountid());
                    long amount1 = Long.parseLong(c1.getAddress());
                    double aaa1 = a.getAmount() * 0.05;
                    String result11 = String.valueOf(aaa1).split("\\.")[0];
                    long feeCancel = Long.parseLong(result11);
                    m.insertTransfer1(feeCancel, "Free buyer cancel", 3, c1.getAccountid());
                    dd.updateMoney(amount1 + feeCancel, c1.getAccountid());
                    String formattedNumber2 = formatter.format(amount1 + feeCancel);

                    //===============Notification=============== bam - mua
                    NotificationEndpoint.sendMessageToAccount(c.getAccountid() + "", "Buyer " + c.getUserName() + " canceled the order " + p.getId() + ""
                            + ", your balance is refunded " + formattedNumber + "VND after tax 10%, total now: " + formattedNumber1);
                    NotificationEndpoint.sendMessageToAccount(c1.getAccountid() + "", "You have canceled your order from " + c1.getUserName() + ", your balance is refunded " + formattedNumber + "VND after tax 10%, total now: " + formattedNumber2);

                    /////===============Update product =======================
                    no.insertNotification(c.getAccountid(), "Buyer " + c.getUserName() + " canceled the order " + p.getId() , 3);
                    no.insertNotification(c1.getAccountid(), "You have canceled your order from " + c1.getUserName() + ", your balance is refunded " + formattedNumber + "VND after tax 10%, total now: " + formattedNumber2, 3);
                    d.updateStatusProduct1(p.getId());
                    queue.getMyrequest().pop();
                }

            }

        }
                    response.sendRedirect("home");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
