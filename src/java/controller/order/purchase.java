/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import controller.home.NotificationEndpoint;
import dal.accountDAO;
import dal.moneyDAO;
import dal.notificationDAO;
import dal.orderDAO;
import dal.productDAO;
import dal.productImageDAO;
import dal.reportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.account;
import model.order;
import model.product;
import model.queue;
import model.report;
import model.trading;

@WebServlet(name = "purchase", urlPatterns = {"/purchase"})
public class purchase extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet purchase</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet purchase at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        productDAO d = new productDAO();
        productImageDAO dd = new productImageDAO();
        String id = request.getParameter("id");
        HttpSession s = request.getSession();
        account a = (account) s.getAttribute("account");

        if (a == null) {
            request.getRequestDispatcher("login.jsp").forward(request, response);

        } else {
            orderDAO o = new orderDAO();
            List<order> o1 = o.getOrderByProductid(id);
            product p = d.getProductbyID(id);

            if (o1 == null) {
                response.sendRedirect("home");
            } else if ((p.getStatus().equalsIgnoreCase("wait"))) {

                response.sendRedirect("home");
                ///TH khác người mua và người bán;
                NotificationEndpoint.sendMessageToAccount(a.getAccountid() + "", "You need to buy to see details");
            } else {
                if (p.getStatus().equalsIgnoreCase("Confirmed")) {
                    response.sendRedirect("home");
                    NotificationEndpoint.sendMessageToAccount(a.getAccountid() + "", "The product has been purchased.");

                } else {

                    if (((o1.get(0).getAccountid().getAccountid() != a.getAccountid()
                            && p.getAccountId().getAccountid() != a.getAccountid()))) {
                        response.sendRedirect("home");
                        NotificationEndpoint.sendMessageToAccount(a.getAccountid() + "", "Products being traded .");
                    } else {

                        reportDAO r = new reportDAO();
                        report re = r.getReportByOrder(o1.get(0).getId());
                        if (re != null) {
                            request.setAttribute("report", re);
                            request.setAttribute("product", p);
                            request.setAttribute("image", dd.listImageByProductId(id));
                            request.getRequestDispatcher("purchase.jsp").forward(request, response);
                        } else {
                            request.setAttribute("product", p);
                            request.setAttribute("image", dd.listImageByProductId(id));
                            request.getRequestDispatcher("purchase.jsp").forward(request, response);
                        }

                    }

                }

            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        productDAO d = new productDAO();
        productImageDAO dd = new productImageDAO();
        accountDAO a = new accountDAO();
        String id = request.getParameter("id");
        orderDAO o = new orderDAO();

        HttpSession s = request.getSession();
        account ac = (account) s.getAttribute("account");
        account b = a.getAccount1(ac.getAccountid() + "");
        product p = d.getProductbyID(id);
        moneyDAO m = new moneyDAO();

//          if (p.getStatus().equalsIgnoreCase("Wait confirm")) {
//            response.sendRedirect("home");
//
//        } else
        if (p.getAccountId().getAccountid() == b.getAccountid()) {
            response.sendRedirect("home");

        } else {

            if ((!("Private".equalsIgnoreCase(p.getStatus())) && p.getStatus().equalsIgnoreCase("Wait"))) {

//                List<order> o1 = o.getOrderByProductid(id);
                NotificationEndpoint.sendMessageToAccount(p.getAccountId().getAccountid() + "", b.getUserName() + " đã mua sản phẩm " + p.getId() + " của bạn.");
                long balance = Long.parseLong(b.getAddress());
                String result = p.getPrice().split("\\.")[0];
                long price = Long.parseLong(result);
                notificationDAO no = new notificationDAO();
                if (balance >= price) {
                    queue.getMyrequest().add(new trading(b.getAccountid(), price));

                    while (!queue.getMyrequest().isEmpty()) {

                        trading aaa = queue.getMyrequest().getFirst();
                        no.insertNotification(aaa.getAccountId(), "You have purchased the order code " + p.getId() + ". Please check the information before confirming", 3);

                        m.insertTransfer(aaa.getAmount(), aaa.getAccountId(), 3);

                        o.insertOrder(aaa.getAmount(), id, aaa.getAccountId());
                        d.updateStatusProduct(id);
                        a.updateMoney(Long.parseLong(a.getAccount(aaa.getAccountId()).getAddress()) - aaa.getAmount(), aaa.getAccountId());
                        ///update product status "purchase"
                        /// update money buyer
                        /// insert order
                        queue.getMyrequest().pop();

                    }
                    request.setAttribute("product", p);
                    request.setAttribute("image", dd.listImageByProductId(id));
                    request.getRequestDispatcher("purchase.jsp").forward(request, response);

                } else {
                    response.sendRedirect("home");
                }

            } else {
                response.sendRedirect("home");
            }

        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
