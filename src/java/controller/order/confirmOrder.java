/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import controller.home.NotificationEndpoint;
import dal.accountDAO;
import dal.moneyDAO;
import dal.notificationDAO;
import dal.orderDAO;
import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.NumberFormat;
import java.util.Locale;
import model.account;
import model.product;
import model.queue;
import model.trading;

/**
 *
 * @author Truong cuaaaaaaaaaaaaaaaaaaan
 */
@WebServlet(name = "confirmOrder", urlPatterns = {"/confirmOrder"})
public class confirmOrder extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet confirmOrder</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet confirmOrder at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        productDAO d = new productDAO();
        product p = d.getProductbyID(id);
        accountDAO dd = new accountDAO();
        account ac = dd.getAccount(p.getAccountId().getAccountid());
        orderDAO o = new orderDAO();
        String result = p.getPrice().split("\\.")[0];
        long price1 = Long.parseLong(result);

        PrintWriter out = response.getWriter();
        out.println(price1);
        double aaa = price1 * 0.95;
        String result1 = String.valueOf(aaa).split("\\.")[0];
        long price = Long.parseLong(result1);
//        out.println(price);

        moneyDAO m = new moneyDAO();

        queue.getMyrequest().add(new trading(ac.getAccountid(), price));
        notificationDAO no = new notificationDAO();

        while (!queue.getMyrequest().isEmpty()) {
            trading a = queue.getMyrequest().getFirst();
            account c = dd.getAccount(a.getAccountId());

            long amount = Long.parseLong(c.getAddress());
            m.insertTransfer(a.getAmount(), 3, c.getAccountid());

            dd.updateMoney(amount + a.getAmount(), c.getAccountid());
            NumberFormat formatter = NumberFormat.getInstance(new Locale("vi", "VN"));
            d.updateStatusProductConfirm(id);
            o.updateOrderStatus(id);
            String formattedNumber = formatter.format(a.getAmount());
            String formattedNumber1 = formatter.format(amount + a.getAmount());
            NotificationEndpoint.sendMessageToAccount(a.getAccountId() + "",
                    "Buyer confirmed, your balance added " + formattedNumber + "VND after tax 5%, total now: " + formattedNumber1);

            no.insertNotification(a.getAccountId(), "Buyer confirmed, your balance added " + formattedNumber + "VND after tax 5%, total now: " + formattedNumber1, 3);

            queue.getMyrequest().pop();
        }

        response.sendRedirect("home");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
