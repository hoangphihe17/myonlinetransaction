/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.profileUser;

import dal.accountDAO;
import dal.messageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;
import model.account;

/**
 *
 * @author Truongwwwwwwwwwwwwwwwww cun
 */
@WebServlet(name = "updateProfile", urlPatterns = {"/updateProfile"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)

public class updateProfile extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {
            HttpSession session = request.getSession();
            account userOn = (account) session.getAttribute("account");

            String realPath = request.getServletContext().getRealPath("/image");
            Files.createDirectories(Paths.get(realPath)); // Tạo thư mục nếu chưa tồn tại
            PrintWriter out = response.getWriter();
            accountDAO a = new accountDAO();
            for (Part part : request.getParts()) {
                if (part != null && part.getSubmittedFileName() != null) {
                    String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                    part.write(Paths.get(realPath, filename).toString()); // Lưu file
//                                out.print(text+"--"+filename+"--"+boxChat+"--"+userOn.getAccountid());
                    a.changeAvt( "image/"+filename, userOn.getAccountid());
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        response.setContentType("text/plain");

        response.sendRedirect("profileUser");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
