/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.profileUser;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "passProfile", urlPatterns = {"/passProfile"})
public class passProfile extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet passProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet passProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        account a = (account) session.getAttribute("account");
        accountDAO d = new accountDAO();
        String old = request.getParameter("oPass");
        String news = request.getParameter("nPass");

        try {
             if (d.check(a.getUserName(), old) == null) {
            request.setAttribute("error", "Password Invalid ,please input again.");
            request.getRequestDispatcher("profileUser").forward(request, response);

        } else {
            d.updatePass1(a.getUserName(), news);
            request.setAttribute("error", "Update password success");
            request.getRequestDispatcher("profileUser").forward(request, response);

        }
        } catch (Exception e) {
             request.setAttribute("error", "please login");
            request.getRequestDispatcher("profileUser").forward(request, response);

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        account a = (account) session.getAttribute("account");
        accountDAO d = new accountDAO();
        String old = request.getParameter("oPass");
        String news = request.getParameter("nPass");

        try {
             if (d.check(a.getUserName(), old) == null) {
            request.setAttribute("error", "Password Invalid ,please input again.");
            request.getRequestDispatcher("profileUser").forward(request, response);

        } else {
            d.updatePass1(a.getUserName(), news);
            request.setAttribute("error", "Update password success");
            request.getRequestDispatcher("profileUser").forward(request, response);

        }
        } catch (Exception e) {
             request.setAttribute("error", "please login");
            request.getRequestDispatcher("profileUser").forward(request, response);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
