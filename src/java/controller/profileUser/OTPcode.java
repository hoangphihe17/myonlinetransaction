/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.profileUser;

import dal.verifyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.account;
import utils.templateMail;

@WebServlet(name = "OTPcode", urlPatterns = {"/OTPcode"})
public class OTPcode extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OTPcode</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OTPcode at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       

    }

    public static String generateRandomNumber(int length) {
        Random random = new Random();
        StringBuilder randomNumber = new StringBuilder();
        for (int i = 0; i < length; i++) {
            randomNumber.append(random.nextInt(10));
        }
        return randomNumber.toString();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        verifyDAO v = new verifyDAO();
        HttpSession session = request.getSession();
        account a = (account) session.getAttribute("account");
        String otp = generateRandomNumber(6);
        String action = request.getParameter("action");

        if (action != null && action.equalsIgnoreCase("forgot")) {
            v.updateOTP(otp, a.getAccountid());
        } else {
            v.insertOTP(otp, a.getAccountid());

        }

        request.setAttribute("error", "check your Email");
        request.getRequestDispatcher("profileUser").forward(request, response);

        String accountMail = "truongvxhe176609@fpt.edu.vn";
        String codeMail = "ppgc nrmq ynyk mhwu";
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        Session session1 = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(accountMail, codeMail);
            }
        });
        try {
            MimeMessage message = new MimeMessage(session1);
            message.setFrom(new InternetAddress(accountMail));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(a.getEmail()));
            message.setSubject("[Kibyhunter Trading] Your news OTP code .");
            templateMail ok = new templateMail();
            message.setContent( ok.sentMail(otp), "text/html");
            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
