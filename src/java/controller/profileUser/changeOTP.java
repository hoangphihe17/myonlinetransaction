/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.profileUser;

import dal.verifyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.account;


@WebServlet(name="changeOTP", urlPatterns={"/changeOTP"})
public class changeOTP extends HttpServlet {
   
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet changeOTP</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet changeOTP at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        
       String o1 = request.getParameter("o1");
       String o2 = request.getParameter("o2");
       String o3 = request.getParameter("o3");
       String o4 = request.getParameter("o4");
       String o5 = request.getParameter("o5");
       String o6 = request.getParameter("o6");
       
       String o7 = request.getParameter("o7");
       String o8 = request.getParameter("o8");
       String o9 = request.getParameter("o9");
       String o10 = request.getParameter("o10");
       String o11 = request.getParameter("o11");
       String o12 = request.getParameter("o12");
       
       String oldOTP =o1+o2+o3+o4+o5+o6;
       String newsOTP =o7+o8+o9+o10+o11+o12;
       
         HttpSession session = request.getSession();
        account a = (account) session.getAttribute("account");
       verifyDAO v = new verifyDAO();
       if(v.checkOTP(oldOTP, a.getAccountid())){
           v.updateOTP(newsOTP, a.getAccountid());
             request.setAttribute("error", "Update OTP success !");
                  request.getRequestDispatcher("profileUser").forward(request, response);

       }else{
           request.setAttribute("error", "OTP invalid input again.");
                  request.getRequestDispatcher("profileUser").forward(request, response);

       }
       
       
    } 

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
