/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.analyze;

import dal.analyzeDAO;
import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import model.account;
import model.order;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Truong ddddddddddddddddddddddddddcun
 */
@WebServlet(name = "excel", urlPatterns = {"/excel"})
public class excel extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet excel</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet excel at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Date ngayHienTai = new Date();
        String id = request.getParameter("id");
        String page = request.getParameter("page");

        // Định dạng ngày theo dạng bạn muốn (ví dụ: "dd/MM/yyyy")
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String ngayxuat = sdf.format(ngayHienTai);
        analyzeDAO dd = new analyzeDAO();

        List<order> listOrder = dd.getAllListOrder(id);

        DecimalFormat formatter = new DecimalFormat("###,###.###");

//        List<account> list = d.OrderRecent();
        int maximum = 2147483647;
        int minimum = 1;

        Random rn = new Random();
        int range = maximum - minimum + 1;
        int randomNum = rn.nextInt(range) + minimum;

        FileOutputStream file = new FileOutputStream("D:\\exelSWP391\\" + "orderList-" + ngayxuat + "-" + Integer.toString(randomNum) + ".xlsx");
      

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet workSheet = workbook.createSheet("1");
            XSSFRow row;
            XSSFCell cell0;
            XSSFCell cell1;
            XSSFCell cell2;
            XSSFCell cell3;
            XSSFCell cell4;
            XSSFCell cell5;
            XSSFCell cell6;

            row = workSheet.createRow(0);

            cell0 = row.createCell(0);
            cell0.setCellValue("USER");
            cell1 = row.createCell(1);
            cell1.setCellValue("EMAIL");
            cell2 = row.createCell(2);
            cell2.setCellValue("STATUS");
            cell3 = row.createCell(3);
            cell3.setCellValue("TRANCTION ID");

            cell4 = row.createCell(4);
            cell4.setCellValue("PRICE");
            cell5 = row.createCell(5);
            cell5.setCellValue("ORDERDATE");
            cell6 = row.createCell(6);
            cell6.setCellValue("SUCCESS");

            int i = 0;

            for (order o : listOrder) {
//                String price = formatter.format();

                i = i + 1;
                row = workSheet.createRow(i);
                cell0 = row.createCell(0);
                cell0.setCellValue(o.getAccountid().getUserName());

                cell1 = row.createCell(1);
                cell1.setCellValue(o.getAccountid().getEmail());

                cell2 = row.createCell(2);
                cell2.setCellValue(o.getProductid().getStatus());

                cell3 = row.createCell(3);
                cell3.setCellValue(o.getProductid().getId());

                cell4 = row.createCell(4);
                cell4.setCellValue(o.getPrice());

                cell5 = row.createCell(5);
                cell5.setCellValue(o.getOrderdate());

                cell6 = row.createCell(6);
                cell6.setCellValue(o.getSuccess());

            }

            workbook.write(file);
            workbook.close();
            file.close();


//        request.setAttribute("mess", "Dowload file successed");
//        request.getRequestDispatcher("manager").forward(request, response);
        response.sendRedirect("analyze?id=" + id + "&page=" + page);

    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
