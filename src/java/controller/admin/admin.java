/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dal.accountDAO;
import dal.analyzeDAO;
import dal.moneyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.account;
import model.deposit;
import model.order;
import model.transfer;
import model.transfer1;
import model.weekDay;
import model.withdraw;

/**
 *
 * @author Truong cun
 * 
 */
@WebServlet(name = "admin", urlPatterns = {"/admin"})
public class admin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet admin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet admin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");

        if (id == null) {
            id = "9999";
        }
//        productDAO d = new productDAO();
        accountDAO a = new accountDAO();
        analyzeDAO dd = new analyzeDAO();
        model.analyze d1 = dd.getAnalyzeOK1();
        model.analyze d2 = dd.getAnalyzeNO1();
        model.analyze d3 = dd.top1Customer1();
        model.analyze d6 = dd.lastTop1Customer1();
        moneyDAO d = new moneyDAO();
        
        int aa = 0;
        try {
            aa = Integer.parseInt(d3.getOrderSuccess());

        } catch (Exception e) {
            System.out.println(e);
        }

        account ac = a.getAccount(aa);
        List<weekDay> d4 = dd.weekly1();
        List<weekDay> d5 = dd.lastWeek1();
        List<model.analyze> d7 = dd.top3Customer1();
        List<account> listTop3 = new ArrayList<>();
        for (int i = 0; i < d7.size(); i++) {
            try {
                aa = Integer.parseInt(d7.get(i).getOrderSuccess());

            } catch (Exception e) {
                System.out.println(e);
            }
            account acc = a.getAccount(aa);
            listTop3.add(acc);
        }
        weekDay d8 = dd.yearAndLastYear1();
//getAllListOrder

////phan trang order
        List<order> listOrder = dd.getAllListOrder1();

        int page, numberPage = 5;
        int size = listOrder.size();
        int num = (size % numberPage == 0 ? (size / numberPage) : ((size / numberPage) + 1));
        String xpage = request.getParameter("page");

        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int e;
        int s;

        s = (page - 1) * numberPage;

        e = Math.min(page * numberPage, size);
        List<order> order = dd.Order(listOrder, s, e);

        
         request.setAttribute("listorder", order);// list phan trang
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        HttpSession session = request.getSession();

        // Lấy giá trị hiện tại của thuộc tính "page" từ session
        Object currentPage = session.getAttribute("page");

        // Nếu currentPage không null, tức là thuộc tính đã tồn tại
        if (currentPage != null) {
            // Xóa thuộc tính đã tồn tại
            session.removeAttribute("page");
        }

        
        
        /////////////////////
////phan trang transfer
        List<transfer1> list4 = d.getAllTransfer1();


        int page1, numberPage1 = 5;
        int size1 = list4.size();
        int num1 = (size1 % numberPage1 == 0 ? (size1 / numberPage1) : ((size1 / numberPage1) + 1));
        String xpage1 = request.getParameter("page1");

        if (xpage1 == null) {
            page1 = 1;
        } else {
            page1 = Integer.parseInt(xpage1);
        }
        int e1;
        int s1;

        s1 = (page1 - 1) * numberPage1;

        e1 = Math.min(page1 * numberPage1, size1);
        List<transfer1> transfer = dd.transferPhanTrang(list4, s1, e1);
        
        
                request.setAttribute("allTransfer", transfer);

                request.setAttribute("page1", page1);
        request.setAttribute("num1", num1);

        // Lấy giá trị hiện tại của thuộc tính "page" từ session
        Object currentPage1 = session.getAttribute("page1");

        // Nếu currentPage không null, tức là thuộc tính đã tồn tại
        if (currentPage1 != null) {
            // Xóa thuộc tính đã tồn tại
            session.removeAttribute("page1");
        }
        
     ////////////////////////   
        

       
        moneyDAO m = new moneyDAO();
        List<deposit> d9 = m.getALlListDeposit1();
        List<withdraw> d10 = m.getALlListWithDraw1();

        // Thiết lập thuộc tính "page" trong session với giá trị mới
        session.setAttribute("page", page);
        session.setAttribute("id", id);

        request.setAttribute("success", d1);
        request.setAttribute("fail", d2);
        request.setAttribute("top1customer", d3);
        request.setAttribute("top1customerInfo", ac);
        request.setAttribute("weekDay", d4);
        request.setAttribute("lastWeek", d5);
        request.setAttribute("lasttop1custlastomer", d6);
        request.setAttribute("top3customer", d7);
        request.setAttribute("year", d8);
        request.setAttribute("top3customerInfo", listTop3);
        request.setAttribute("listOrder", listOrder);
        request.setAttribute("idAcc", id);
        request.setAttribute("deposit", d9);
        request.setAttribute("withdraw", d10);

        request.getRequestDispatcher("admin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
