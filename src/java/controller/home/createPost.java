/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.productDAO;
import dal.productImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import utils.RandomNameGenerator;

@WebServlet(name = "createPost", urlPatterns = {"/createPost"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)

public class createPost extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        productDAO d = new productDAO();
        productImageDAO d1 = new productImageDAO();

        RandomNameGenerator gen = new RandomNameGenerator();
        String productID = gen.generateRandomName(15);

        try {
            String name = request.getParameter("name");
            String fee = request.getParameter("fee");
            String describe = request.getParameter("describe");
            String publicPrivate = request.getParameter("publicPrivate");
            String price = request.getParameter("price");
            String info = request.getParameter("info");
            String privateInfo = request.getParameter("privateInfo");
            // Tạo bài đăng sản phẩm
            d.createPost(productID, name, fee, describe, info, price, privateInfo, publicPrivate, 2);

            // Xử lý tải lên ảnh
            String realPath = request.getServletContext().getRealPath("/image");
            Files.createDirectories(Paths.get(realPath)); // Tạo thư mục nếu chưa tồn tại

            for (Part part : request.getParts()) {
                if (part != null && part.getSubmittedFileName() != null) {
                    String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                    part.write(Paths.get(realPath, filename).toString()); // Lưu file
                    d1.addImage(productID, "image/"+filename); // Thêm hình ảnh vào cơ sở dữ liệu
                }
            }

            response.sendRedirect("home");

        } catch (Exception e) {
            e.printStackTrace(); // Xử lý lỗi một cách có ý nghĩa hơn
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet createPost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet createPost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("home").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
