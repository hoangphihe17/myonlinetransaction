/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.product;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "displayBookmark", urlPatterns = {"/displayBookmark"})
public class displayBookmark extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet displayBookmark</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet displayBookmark at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Đọc cookie từ request
        Cookie[] cookies = request.getCookies();
        String favoriteListStr = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("favorite1".equals(cookie.getName())) {
                    favoriteListStr = cookie.getValue();
                    break;
                }
            }
        }

        productDAO d = new productDAO();

// Phân tích dữ liệu từ chuỗi favoriteListStr, nếu có
        List<product> favoriteProducts = new ArrayList<>();
        if (favoriteListStr != null && !favoriteListStr.isEmpty()) {
           

            String[] productEntries = favoriteListStr.split(":");

            // Đảm bảo rằng có đủ thông tin cho mỗi sản phẩm
            for (int i = 0; i < productEntries.length; i++) {
                product p = d.getProductbyID(productEntries[i]);
                favoriteProducts.add(p);

            }

        }

// Gửi danh sách sản phẩm yêu thích đến JSP
        request.setAttribute("favoriteProducts", favoriteProducts);
        request.getRequestDispatcher("bookmark.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
