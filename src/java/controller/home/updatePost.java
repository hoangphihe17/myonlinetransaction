/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.productDAO;
import dal.productImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "updatePost", urlPatterns = {"/updatePost"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class updatePost extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updatePost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updatePost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//PrintWriter out = response.getWriter();
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
//
//        String id = request.getParameter("id");
//
//        String name = request.getParameter("uTitle");
//        String fee = request.getParameter("uFee");
//        String describe = request.getParameter("uDiscription");
//        String publicPrivate = request.getParameter("uPublicPrivate");
//        String price = request.getParameter("uPrice");
//        String info = request.getParameter("uContact");
//        String privateInfo = request.getParameter("uPrivateInfo");
//        String[] uFileImages = request.getParameterValues("uFileImage_normal");
//
//        String realPath = request.getServletContext().getRealPath("/image");
//        Files.createDirectories(Paths.get(realPath)); // Tạo thư mục nếu chưa tồn tại
//
//        productDAO d = new productDAO();
//        productImageDAO d1 = new productImageDAO();
//        d.updatePost(id, name, fee, describe, info, price, privateInfo, publicPrivate);
//                boolean check = false;
//
//        for (Part part : request.getParts()) {
//
//            if (part != null && part.getSubmittedFileName() != null) {
//               
//
//                String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
//                part.write(Paths.get(realPath, filename).toString()); // Lưu file
//
//                d1.addImage(id, "image/" + filename); // Thêm hình ảnh vào cơ sở dữ liệu
//            } else if (uFileImages != null && part == null) {
//               
//                    if (check == false) {
//                    d1.deleteImage(id);
//                    check = true;
//                }
//               
//                for (String o : uFileImages) {
//                    d1.addImage(id, o); // Thêm hình ảnh vào cơ sở dữ liệu
//
//                }
//            }
//        }
        String id = request.getParameter("id");
        String name = request.getParameter("uTitle");
        String fee = request.getParameter("uFee");
        String description = request.getParameter("uDiscription");
        String publicPrivate = request.getParameter("uPublicPrivate");
        String price = request.getParameter("uPrice");
        String info = request.getParameter("uContact");
        String privateInfo = request.getParameter("uPrivateInfo");
        String[] uFileImages = request.getParameterValues("uFileImage_normal");

// Update post information
        productDAO productDao = new productDAO();
        productDao.updatePost(id, name, fee, description, info, price, privateInfo, publicPrivate);

// Handle image directory
        String realPath = request.getServletContext().getRealPath("/image");
        Files.createDirectories(Paths.get(realPath));

// Handle new uploaded images
        productImageDAO productImageDao = new productImageDAO();
        boolean imageUpdated = false;

        for (Part part : request.getParts()) {
            if (part != null && part.getSubmittedFileName() != null && uFileImages == null ) {
                if (!imageUpdated) {
                    productImageDao.deleteImage(id); // Delete old images once
                    imageUpdated = true;
                }
                try {
                     String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                    part.write(Paths.get(realPath, filename).toString()); // Save file
                    productImageDao.addImage(id, "image/" + filename); // Add image to database
                } catch (Exception e) {
                }
                   
               
            }
        }

// Handle previously uploaded images if no new images were uploaded
        if (uFileImages != null && !imageUpdated) {

            productImageDao.deleteImage(id);
            for (String filePath : uFileImages) {
                productImageDao.addImage(id, filePath); // Add image to database
            }
        }

        response.sendRedirect("home");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
