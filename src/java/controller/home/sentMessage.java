package controller.home;

import dal.messageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;
import model.account;

@WebServlet(name = "sentMessage", urlPatterns = {"/sentMessage"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)

public class sentMessage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet sentMessage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet sentMessage at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

// Khởi tạo DAO và lấy thông tin từ session và request
        messageDAO m = new messageDAO();
        String receive = request.getParameter("receive");
        HttpSession session = request.getSession();
        account userOn = (account) session.getAttribute("account");
        String text = request.getParameter("messageText");
        int boxChat = Integer.parseInt(request.getParameter("boxChat"));
        String realPath = request.getServletContext().getRealPath("/image");
        Files.createDirectories(Paths.get(realPath)); // Tạo thư mục nếu chưa tồn tại

        String filename = null;
        boolean isTextSent = false;
        boolean isFileSent = false;

        try {
            // Xử lý các part trong request
            for (Part part : request.getParts()) {
                // Kiểm tra nếu là một phần của file
                if (part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                    filename = Paths.get(part.getSubmittedFileName()).getFileName().toString(); // Lấy tên file
                    part.write(Paths.get(realPath, filename).toString()); // Lưu file
                    isFileSent = true; // Đánh dấu đã gửi file
                }
            }

            // Xử lý logic dựa trên việc gửi text và/hoặc file
            if (text != null && !text.isEmpty() && isFileSent) {
                // Gửi cả văn bản và file
                m.addMess(boxChat, userOn.getAccountid(), filename, text);
            } else if (text != null && !text.isEmpty()) {
                // Chỉ gửi văn bản
                m.addMess1(boxChat, userOn.getAccountid(), text);
            } else if (isFileSent) {
                // Chỉ gửi file
                m.addMess2(boxChat, userOn.getAccountid(), filename);
            } else {
                m.addMess1(boxChat, userOn.getAccountid(), "<span class=\"like_btn\" \"><i class=\"fas fa-thumbs-up\" style=\"font-size:24px;\"></i></span>\n"+"");

            }

            response.getWriter().write("Message sent successfully");
        } catch (Exception e) {
            e.printStackTrace(response.getWriter()); // In lỗi để debug
            response.getWriter().write("Error sending message.");
        }

// Chuyển hướng trở lại trang chat, nếu cần
        response.sendRedirect("chat?receive=" + receive);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
