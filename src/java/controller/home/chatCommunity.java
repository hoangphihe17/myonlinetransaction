/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.accountDAO;
import dal.chatCommunityDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Paths;
import model.account;
import model.queue;
import model.trading;

@WebServlet(name = "chatCommunity", urlPatterns = {"/chatCommunity"})
public class chatCommunity extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet chatCommunity</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet chatCommunity at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        chatCommunityDAO d = new chatCommunityDAO();
        HttpSession session = request.getSession();
        accountDAO a = new accountDAO();

        account userOn = (account) session.getAttribute("account");
        request.setAttribute("moneyCustomer", a.getAccount(userOn.getAccountid()));

        request.setAttribute("list", d.getAllList());
        request.getRequestDispatcher("chatCommunity.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String chat = request.getParameter("chatMessage");
        chatCommunityDAO d = new chatCommunityDAO();
        HttpSession s = request.getSession();
                accountDAO ddd = new accountDAO();

        account a = (account) s.getAttribute("account");
        if (a == null) {
            request.setAttribute("error", "Please login before create new chat !");
            request.getRequestDispatcher("login.jsp").forward(request, response);

        } else {

            queue.getMyrequest().add(new trading(a.getAccountid(), 5000));
            accountDAO dd = new accountDAO();

            while (!queue.getMyrequest().isEmpty()) {
                trading aa = queue.getMyrequest().getFirst();
                account c = dd.getAccount(aa.getAccountId());

                long amount = Long.parseLong(c.getAddress());
                if ((amount - aa.getAmount()) >= 0) {
                    dd.updateMoney(amount - aa.getAmount(), c.getAccountid());
                    d.addChatCommunity(a.getAccountid(), a.getUserName() + ": " + chat);
                } else {
                    request.setAttribute("error", "Your balance is not enough. ");
                }
                queue.getMyrequest().pop();

            }
        request.setAttribute("moneyCustomer", ddd.getAccount(a.getAccountid()));


            request.setAttribute("list1111111", d.getAllList());
            request.getRequestDispatcher("chatCommunity.jsp").forward(request, response);
//            request.getRequestDispatcher("home.jsp").forward(request, response);


        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
