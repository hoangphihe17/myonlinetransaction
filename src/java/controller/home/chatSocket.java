
package controller.home;


import com.google.gson.Gson;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.server.ServerEndpoint;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import jakarta.websocket.Session;
import java.nio.ByteBuffer;
/**
 *
 * @author Truong cusddddssssssssssssn
 */
@ServerEndpoint("/chat")

public class chatSocket {
    
    private static Set<Session> userSessions = Collections.newSetFromMap(new ConcurrentHashMap<Session, Boolean>());

    @OnOpen
    public void onOpen(Session curSession) {
        userSessions.add(curSession);
    }

    @OnClose
    public void onClose(Session curSession) {
        userSessions.remove(curSession);
    }

    @OnMessage
    public void onMessage(String message, Session userSession) {
        for (Session ses : userSessions) {
            ses.getAsyncRemote().sendText(message);
        }
    }
    
    @OnMessage
    public void onMessage(ByteBuffer buffer, boolean last, Session userSession) {
        byte[] data = new byte[buffer.remaining()];
        buffer.get(data);
        for (Session ses : userSessions) {
            ses.getAsyncRemote().sendBinary(ByteBuffer.wrap(data));
        }
    }
}
