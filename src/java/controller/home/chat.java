/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dal.accountDAO;
import dal.messageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.account;
import model.boxChat;
import model.message;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "chat", urlPatterns = {"/chat"})
public class chat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet chat</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet chat at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        messageDAO m = new messageDAO();
        try {
            int receive = Integer.parseInt(request.getParameter("receive"));
            HttpSession s = request.getSession();
            account a = (account) s.getAttribute("account");
            boxChat b = m.checkBoxChat(a.getAccountid(), receive);
            boxChat bb = m.checkBoxChat(receive, a.getAccountid());

            accountDAO ac = new accountDAO();

            if (b == null && bb == null) {
                m.addBoxChat(a.getAccountid(), receive);
                boxChat b1 = m.checkBoxChat(a.getAccountid(), receive);
                List<message> list = m.getMessageByBoxChat(b1.getId());
               

                request.setAttribute("list", list);
                account recei = ac.getAccount(receive);
                request.setAttribute("receive", recei);
                request.setAttribute("boxChat", b1.getId());

                request.getRequestDispatcher("chat.jsp").forward(request, response);

            } else {
                if (b != null) {
                    List<message> list = m.getMessageByBoxChat(b.getId());
                    request.setAttribute("list", list);
//                    List<message> list1 = m.getMessageByBoxChat1(b.getId());
//                request.setAttribute("image", list1);
                    account recei = ac.getAccount(receive);
                    request.setAttribute("receive", recei);
                    request.setAttribute("boxChat", b.getId());

                    request.getRequestDispatcher("chat.jsp").forward(request, response);
                } else {
                    List<message> list = m.getMessageByBoxChat(bb.getId());
                    request.setAttribute("list", list);
//                    List<message> list1 = m.getMessageByBoxChat1(bb.getId());
//                request.setAttribute("image", list1);
                    account recei = ac.getAccount(receive);
                    request.setAttribute("receive", recei);
                    request.setAttribute("boxChat", bb.getId());

                    request.getRequestDispatcher("chat.jsp").forward(request, response);
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
