/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.regex.Pattern;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "changePass", urlPatterns = {"/changePass"})
public class changePass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet changePass</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet changePass at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("user");
        request.setAttribute("user", user);
        request.getRequestDispatcher("changePass.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("username");
        String pass1 = request.getParameter("password");
        String captcha = request.getParameter("captcha");

        String pass = pass1.trim();

        accountDAO dao = new accountDAO();

        try {
            String captcha_txt = request.getSession().getAttribute("captcha").toString();

        if (!isValidPassword(pass)) {
            request.setAttribute("error", "Pass must contains [A-Za-z0-9] and special character");
            request.setAttribute("user", user);

            request.getRequestDispatcher("changePass.jsp").forward(request, response);

        } else if (!captcha_txt.equals(captcha)) {
            request.setAttribute("error", "Captcha must not null and match");
            request.setAttribute("user", user);

            request.getRequestDispatcher("changePass.jsp").forward(request, response);

        } else {
                    HttpSession s = request.getSession();


            if (s.getAttribute("account") != null) {
                dao.updatePass1(user, pass);
                request.setAttribute("error", "Update password success");
                s.removeAttribute("account");
                request.getRequestDispatcher("login.jsp").forward(request, response);

            } else {
                request.setAttribute("error", "You can update only 1 times");
                request.getRequestDispatcher("login.jsp").forward(request, response);

            }
        }
        } catch (Exception e) {
            System.out.println(e); 
        }

    }

    public static boolean isValidPassword(String password) {
        // Độ dài tối thiểu là 8 ký tự
        if (password.length() > 3 && password.length() < 8) {
            return false;
        }

        // Ít nhất một chữ cái viết hoa
        if (!Pattern.compile("[A-Z]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một chữ cái thường
        if (!Pattern.compile("[a-z]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một số
        if (!Pattern.compile("[0-9]").matcher(password).find()) {
            return false;
        }

        // Ít nhất một ký tự đặc biệt (có thể thay đổi dấu \\W để bao gồm các ký tự đặc biệt khác)
        if (!Pattern.compile("\\W").matcher(password).find()) {
            return false;
        }

        // Nếu mọi tiêu chí đều đáp ứng, trả về true
        return true;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
