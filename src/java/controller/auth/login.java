/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.account;
import utils.Validate;

/**
 *
 * @author Truong cussssssssssssssssssn
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("login.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        Validate validate = new Validate();
        String user = request.getParameter("username");
        String pass = request.getParameter("password");
        String captcha = request.getParameter("captcha");
        String remember = request.getParameter("remember");

        Cookie u = new Cookie("user", user);
        Cookie p = new Cookie("pass", pass);
        Cookie r = new Cookie("remember", remember);

        if (remember != null) {
            u.setMaxAge(60 * 60 * 24 * 7);
            p.setMaxAge(60 * 60 * 24 * 7);
            r.setMaxAge(60 * 60 * 24 * 7);
        } else {
            u.setMaxAge(0);
            p.setMaxAge(0);
            r.setMaxAge(0);
        }
        response.addCookie(u);
        response.addCookie(p);
        response.addCookie(r);

        accountDAO ac = new accountDAO();

        account a = ac.check(user, pass);
        if (a == null) {
            request.setAttribute("error", "Account not exits dmmmmm");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {

            String captcha_txt = request.getSession().getAttribute("captcha").toString();
            if (captcha_txt.equals(captcha)) {

                if (a.getCode().equalsIgnoreCase("0")) {
                    HttpSession s = request.getSession();

                    s.setAttribute("account", a);
                    response.sendRedirect("home");
                } else {
                    request.setAttribute("user", user);
                    request.getRequestDispatcher("LoginNotAuth?user=" + user).forward(request, response);

                }

            } else {
                request.setAttribute("error", "Captcha code must not null and match");
                request.getRequestDispatcher("login.jsp").forward(request, response);

            }

        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
