/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.vnPay;

import dal.accountDAO;
import dal.moneyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import model.account;

/**
 *
 * @author Truong cun
 */
@WebServlet(name = "deposit", urlPatterns = {"/deposit"})
public class deposit extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet deposit</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet deposit at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        PrintWriter out = response.getWriter();
//        out.print("heeloo");
//http://localhost:9999/swp391/deposit?vnp_Amount=2222200&vnp_BankCode=NCB&vnp_BankTranNo=VNP14308966&vnp_CardType=ATM&vnp_OrderInfo=Thanh+toan+don+hang%3A65417724&vnp_PayDate=20240229093234&vnp_ResponseCode=00&vnp_TmnCode=CV84IH6R&vnp_TransactionNo=14308966&vnp_TransactionStatus=00&vnp_TxnRef=65417724&vnp_SecureHash=26a7684de0a080b53e38a495814b6fe9ff62d9896a0943ede7cfb25007aa76c602fe5fd684d5893d5bb7e3dcba41fc03ea1090b7a725a34cde54227c9d9208a8
        String amount = request.getParameter("vnp_Amount");
//        String bank = request.getParameter("vnp_BankCode");
//        String type = request.getParameter("vnp_CardType"); // atm , the ,....
//        String orderInfo = request.getParameter("vnp_OrderInfo");
        String date = request.getParameter("vnp_PayDate");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime dateTime = LocalDateTime.parse(date, inputFormatter);
        // Định dạng lại LocalDateTime
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = dateTime.format(outputFormatter);
        /// out.print(bank+"--"+amount+"--"+type+"--"+orderInfo+"--"+date+"--"+transaction);
        moneyDAO m = new moneyDAO();
        double money = Double.parseDouble(amount);
        
         HttpSession session = request.getSession();
         account c = (account) session.getAttribute("account");
        m.deposit((money / 100), formattedDateTime, c.getAccountid());

        accountDAO a = new accountDAO();
        double moneyAccount = 0;
        if (c.getAddress() == null) {/// may nua sua thanh code
            moneyAccount = 0;
        } else {
            moneyAccount = Double.parseDouble(c.getAddress());

        }

        a.updateMoney((moneyAccount + (money / 100)), c.getAccountid());
        
        response.sendRedirect("home");

    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
